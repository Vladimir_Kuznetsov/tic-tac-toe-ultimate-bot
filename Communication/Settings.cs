﻿using System;
using System.Collections.Generic;

namespace AIGames.UltimateTicTacToe.StarterBot.Communication
{
	public class Settings
	{
		public PlayerName YourBot { get; set; }
		public PlayerName OppoBot
		{
			get
			{
				switch (YourBot)
				{
					case PlayerName.Player1: return PlayerName.Player2;
					case PlayerName.Player2: return PlayerName.Player1;
					case PlayerName.None:
					default: return PlayerName.None;
				}
			}
		}

		public bool Apply(IInstruction instruction)
		{
			if (Mapping.ContainsKey(instruction.GetType()))
			{
				Mapping[instruction.GetType()].Invoke(instruction, this);
				return true;
			}
			return false;
		}

		private static Dictionary<Type, Action<IInstruction, Settings>> Mapping = new Dictionary<Type, Action<IInstruction, Settings>>()
		{
			{ typeof(YourBotInstruction), (instruction, settings) =>{ settings.YourBot = ((YourBotInstruction)instruction).Name; }},
		};

        public void Configure(LimitedVal[] args)
        {
            this.SPECIAL_STRATEGY_BONUS_K = args[0];
            this.SCORE_TABLE_POW_K = args[1];
            this.MINIMAX_EVALUATION_K = args[2];
            this.SHRINK_K_BASIC = args[3];
            this.SHRINK_K_BLOCK = args[4];
            this.PLAYER_VALUE_K = args[5];
            this.PLAYER_VALUE_POW_K = args[6];
            this.STRATEGY_LINE_K = args[7];
            this.STRATEGY_LINE_POW_K = args[8];
            this.SCORE_TABLE_MY_WEIGHT_K = args[9];
            this.TRAP_K = args[10];
        }
        
        public LimitedVal SPECIAL_STRATEGY_BONUS_K = new LimitedVal(0.1f, 0.8f, 0.2f); // 0.2f * GAME_OVER_VALUE
        public LimitedVal SCORE_TABLE_POW_K = new LimitedVal(0.5f, 1f, 0.8f);          // 0.8f
        public LimitedVal MINIMAX_EVALUATION_K = new LimitedVal(1f, 3f, 1.2f);         // 1.2f
        public LimitedVal SHRINK_K_BASIC = new LimitedVal(0.5f, 1f, 0.8f);             // 0.8f
        public LimitedVal SHRINK_K_BLOCK = new LimitedVal(1f, 3f, 1.2f);               // 1.2f
        public LimitedVal PLAYER_VALUE_K = new LimitedVal(0.1f, 0.9f, 0.3f);           // 0.3f
        public LimitedVal PLAYER_VALUE_POW_K = new LimitedVal(0.1f, 0.8f, 0.4f);       // 0.4f
        public LimitedVal STRATEGY_LINE_K = new LimitedVal(0.1f, 1f, 0.2f);            // 0.2f
        public LimitedVal STRATEGY_LINE_POW_K = new LimitedVal(0.9f, 2f, 1.4f);        // 1.4f
        public LimitedVal SCORE_TABLE_MY_WEIGHT_K = new LimitedVal(0.85f, 1f, 0.9f);   // 0.9f
        public LimitedVal TRAP_K = new LimitedVal(0.5f, 3f, 2f);                       // 2f * GAME_OVER_VALUE
	}
}
