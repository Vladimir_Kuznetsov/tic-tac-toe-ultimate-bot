﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;

namespace AIGames.UltimateTicTacToe.StarterBot.Communication
{
	public class ConsolePlatform : IDisposable
	{
		/// <summary>Runs the bot.</summary>
		public static void Run(IBot bot)
		{
			using (var platform = new ConsolePlatform())
			{
				platform.DoRun(bot);
			}
		}

        public static Settings bufferSettings = new Settings();
        public static GameState bufferGame = new GameState();

		/// <summary>The reader.</summary>
		protected TextReader Reader { get; set; }
		/// <summary>The reader.</summary>
		protected TextWriter Writer { get; set; }
		/// <summary>The reader.</summary>
		protected TextWriter Logger { get; set; }

		/// <summary>Constructs a console platform with Console.In and Console.Out.</summary>
		protected ConsolePlatform() : this(Console.In, Console.Out, Console.Error) { }

		/// <summary>Constructs a console platform.</summary>
		protected ConsolePlatform (TextReader reader, TextWriter writer, TextWriter logger)
		{
			if (reader == null) { throw new ArgumentNullException("reader"); }
			if (writer == null) { throw new ArgumentNullException("writer"); }
			if (logger == null) { throw new ArgumentNullException("logger"); }

			this.Reader = reader;
			this.Writer = writer;
			this.Logger = logger;
		}

		/// <summary>Runs it all.</summary>
		public void DoRun(IBot bot)
		{
			if (bot == null) { throw new ArgumentNullException("bot"); }
            if (Reader == null) { throw new ArgumentNullException("reader"); }
            
            Stopwatch timer = new Stopwatch();
            int movePeriod = int.MaxValue;

            ConcurrentQueue<string> commands = new ConcurrentQueue<string>();

            // Infinite read from the stdin
            new Thread(async () =>
            {
                Thread.CurrentThread.IsBackground = true;
                while (true)
                {
                    string r = await Console.In.ReadLineAsync();
                    if (r != null && !r.Equals(""))
                        commands.Enqueue(r);
                }
            }).Start();

            IEnumerator doInBackground = bot.DoInBackground().GetEnumerator();

            while (true)
            {
                string cmdString;
                if (commands.TryDequeue(out cmdString))
                {
                    var instruction = Instruction.Parse(cmdString);
                    if (instruction != null)
                    {
                        int timeToMove;
                        ProcessInstruction(bot, instruction, out timeToMove);
                        if (timeToMove > 0)
                        {
                            movePeriod = 0;
                            timer.Restart();
                        }
                    }
                }
                    
                // Let the bot work during the opponent move
                if (!doInBackground.MoveNext())
                    doInBackground = bot.DoInBackground().GetEnumerator();
                Object obj = doInBackground.Current;

                // Ask a bot for response when it's time
                if (timer.Elapsed.TotalMilliseconds >= movePeriod)
                {
                    timer.Reset();
                    movePeriod = int.MaxValue;

                    try
                    {
                        var response = bot.GetResponse();
                        Writer.WriteLine(response.Move);
                        if (!String.IsNullOrEmpty(response.Log))
                        {
                            Logger.WriteLine(response.Log);
                        }
                    }
                    catch (Exception x)
                    {
                        Writer.WriteLine(new MoveInstruction(Move.Center));
                        Logger.WriteLine(x);
                    }
                }
            }
		}

        /// <summary>Apply instruction.</summary>
        public void ProcessInstruction (IBot bot, IInstruction instruction, out int timeToMove)
        {
            timeToMove = -1;

            if (bufferSettings.Apply(instruction))
            {
                bot.ApplySettings(bufferSettings);
            }
            else if (bufferGame.Apply(instruction)) { }
            else if (instruction is RequestMoveInstruction)
            {
                bot.Update(bufferGame);
                timeToMove = (int)((RequestMoveInstruction)instruction).Time.TotalMilliseconds;
            }
        }

		#region IDisposable

		/// <summary>Dispose the console platform.</summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Dispose the console platform.</summary>
		protected virtual void Dispose(bool disposing)
		{
			if (!m_IsDisposed)
			{
				if (disposing)
				{
					this.Reader.Dispose();
					this.Writer.Dispose();
				}
				m_IsDisposed = true;
			}
		}

		/// <summary>Destructor</summary>
		~ConsolePlatform() { Dispose(false); }

		private bool m_IsDisposed = false;

		#endregion
	}
}
