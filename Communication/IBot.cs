﻿using System;
using System.Collections;

namespace AIGames.UltimateTicTacToe.StarterBot.Communication
{
	public interface IBot : ICloneable
	{
		void ApplySettings(Settings settings);
		void Update(GameState state);
        IEnumerable DoInBackground();
		BotResponse GetResponse();
        string GetName();

        Settings Settings { get; set; }
        GameState Game { get; set; }
    }
}
