﻿using System;
using System.Diagnostics;
using System.Linq;

namespace AIGames.UltimateTicTacToe.StarterBot.Communication
{
	public struct MacroboardInstruction : IInstruction
	{
		public MacroboardInstruction(bool[] _states)
		{
			m_States = _states;
		}

		public bool[] States { get { return m_States; } }
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly bool[] m_States;

		public override string ToString() 
		{
			return String.Format("update game field {0}", m_States.ToString());
		}

		internal static IInstruction Parse(string[] splited)
		{
			return new MacroboardInstruction(MacroBoard.ParseStates(splited[3]));
		}
	}
}
