﻿namespace AIGames.UltimateTicTacToe.StarterBot.Communication
{
	public enum PlayerName : byte
	{
		None = 0,
		Player1 = 1,
		Player2 = 2,
	}
}
