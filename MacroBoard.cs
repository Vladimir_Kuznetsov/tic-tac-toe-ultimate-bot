﻿using System;
using System.Linq;
using System.Collections.Generic;
using AIGames.UltimateTicTacToe.StarterBot.Communication;

namespace AIGames.UltimateTicTacToe.StarterBot
{
	public class MacroBoard : ICloneable
	{

        public TinyBoard[] boards = new TinyBoard[9];
        public static float GAME_OVER_WEIGHT = 0.3f;

        /// <summary>Creates an independent copy of this object.</summary>
        public object Clone ()
        {
            MacroBoard newMacro = new MacroBoard();
            for (int i = 0; i < 9; i++)
                newMacro.boards[i] = this.boards[i].Clone() as TinyBoard;
            return newMacro;
        }

        /// <summary>Initializes the macro board values.</summary>
        public MacroBoard (byte[,] meta = null)
        {
            if (meta == null)
            {
                meta = new byte[9, 9];
            }

            byte[] empty = new byte[9];

            for (int i = 0; i < 9; i++)
                this.boards[i] = TinyBoard.New(empty, i);

            this.SetMetaBoard(meta);
            Strategy.SetDefaultStrategy(this);
        }

        /// <summary>Returns a cell from a meta board.</summary>
        public byte GetCell(int x, int y)
        {
            if (x > 9 || y > 9 || x < 0 || y < 0)
                throw new IndexOutOfRangeException("x, y");

            int boardNum = (y / 3) * 3 + x / 3;
            return this.boards[boardNum].board[(y % 3) * 3 + (x % 3)];
        }

        /// <summary>Applies a new value to a cell.</summary>
        public void SetCell(byte player, int x, int y)
        {
            if (x > 9 || y > 9 || x < 0 || y < 0)
                throw new IndexOutOfRangeException("x, y");

            int boardNum = (y / 3) * 3 + x / 3;
            this.boards[boardNum].board[(y % 3) * 3 + (x % 3)] = player;
        }

        /// <summary>Updates meta board and other correlated values.</summary>
        public void SetMetaBoard (byte[,] meta)
        {
            if (meta.GetLength(0) != 9 || meta.GetLength(1) != 9)
                throw new IndexOutOfRangeException("newStates");

            for (int i = 0; i < 9; i++)
            {
                byte[] tinyBoard = Utils.GetTiny(meta, i);
                if (!Utils.BoardsAreEqual(tinyBoard, this.boards[i].board))
                {
                    this.boards[i].board = tinyBoard;
                }
            }
        }

        /// <summary>Updates macro board states.</summary>
        public void SetStates (bool[] newStates)
        {
            if (newStates.Length != 9)
                throw new IndexOutOfRangeException("newStates");

            for (int i = 0; i < 9; i++)
            {
                this.boards[i].isAvailable = newStates[i];
            }
        }

        /// <summary>Updates strategy vals for each board.</summary>
        public void SetMacroStrategies (float[] macroStrategys)
        {
            if (macroStrategys.Length != 9)
                throw new IndexOutOfRangeException("macroStrategys");

            for (int i = 0; i < 9; i++)
            {
                this.boards[i].macroStrategy = macroStrategys[i];
            }
        }

        /// <summary>Parses a meta board.</summary>
		public static byte[,] ParseMeta(string str)
        {
            var bytes = str
                .Split(',')
                .Select(digit => byte.Parse(digit))
                .ToArray();

            var board = new byte[9, 9];

            var index = 0;
            for (var y = 0; y < 9; y++)
            {
                for (var x = 0; x < 9; x++)
                {
                    board[y, x] = bytes[index++];
                }
            }
            return board;
        }

        /// <summary>Parses Macroboard status list from standard input string.</summary>
        public static bool[] ParseStates(string str)
		{
			return str
				.Split(',')
				.Select(digit => int.Parse(digit) == -1)
				.ToArray();
        }

        /// <summary>Calculates macroValue for each board.</summary>
        public void UpdateWeights(int preferredPlayer, Settings settings)
        {
            float[] notNormalizedSum = new float[9];
            List<int> gameOverBoards = new List<int>();

            // Get not normalized average weight values
            for (int i = 0; i < 9; i++)
            {
                if (this.boards[i].winner == -1)
                {
                    for (int j = 0; j < 9; j++)
                    {

                        // Weight is proportional to total values of next possible boards
                        if (this.boards[i].board[j] == 0)
                        {
                            notNormalizedSum[i] += (float) Math.Sqrt(TinyBoard.GetNotNormalizedTotalValue(this.boards[j], preferredPlayer, settings));
                        }
                    }
                    notNormalizedSum[i] /= this.boards[i].GetEmptyCells().Count;

                    // Weight is inversly proportional to the value of the tiny board
                    // To make this correlation more important we use Math.Pow
                    notNormalizedSum[i] /= (float) Math.Pow(TinyBoard.GetNotNormalizedTotalValue(this.boards[i], preferredPlayer, settings), 1.2);
                } else
                {
                    notNormalizedSum[i] = 1 / TinyBoard.GAME_OVER_VALUE;
                    gameOverBoards.Add(i);
                }
            }

            // Set current game over value if there is one
            if (gameOverBoards.Count > 0)
            {
                GAME_OVER_WEIGHT = notNormalizedSum[gameOverBoards[0]];

                // If there are boards where we have to block opponent moves, lower GAME_OVER_WEIGHT
                // This means that using those cells becomes totally unacceptable.
                bool isDangerousGame = false;
                for (int i = 0; i < 9; i++)
                    isDangerousGame |= (this.boards[i].tinyStrategy == TinyStrategy.blockOpponent);
                if (isDangerousGame)
                {
                    GAME_OVER_WEIGHT /= 2;
                    foreach (int b in gameOverBoards)
                        notNormalizedSum[b] = GAME_OVER_WEIGHT;
                }
            }

            // Normalize all weights so they sum up to N
            Utils.NormalizeValues(ref notNormalizedSum, 9);

            // Apply normalized weights to boards
            for (int i = 0; i < 9; i++)
                this.boards[i].totalValue = notNormalizedSum[i];
        }

        /// <summary>Returns meta board.</summary>
        public byte[,] GetMeta()
        {
            byte[,] meta = new byte[9, 9];
            for (int x = 0; x < 9; x++)
                for (int y = 0; y < 9; y++)
                    meta[x, y] = this.GetCell(x, y);
            return meta;
        }
    }
}
