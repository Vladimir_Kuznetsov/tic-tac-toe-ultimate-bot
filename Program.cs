﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#if IS_DEBUG
using System.Windows.Forms;
#endif

namespace AIGames.UltimateTicTacToe.StarterBot
{
	public class Program
	{
        public static void Main()
		{
#if IS_DEBUG
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DebugUI());

#else
            var arg0 = new LimitedVal(0.1f, 0.4f, 0.40f);
            var arg1 = new LimitedVal(0.5f, 1.0f, 0.60f);
            var arg2 = new LimitedVal(1.0f, 3.0f, 1.45f);
            var arg3 = new LimitedVal(0.5f, 1.0f, 0.62f);
            var arg4 = new LimitedVal(1.0f, 3.0f, 2.59f);
            var arg5 = new LimitedVal(0.1f, 0.9f, 0.53f);
            var arg6 = new LimitedVal(0.1f, 0.8f, 0.10f);
            var arg7 = new LimitedVal(0.1f, 1.0f, 0.30f);
            var arg8 = new LimitedVal(0.9f, 2.0f, 0.97f);
            var arg9 = new LimitedVal(0.85f, 1.0f, 0.93f);
            var arg10 = new LimitedVal(0.5f, 3.0f, 0.50f);

            var args = new LimitedVal[] { arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10 };
            Communication.ConsolePlatform.Run(new Starter(args));
#endif
        }
    }
}
