﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIGames.UltimateTicTacToe.StarterBot
{
    class Line : IComparable
    {
        public int[] cells;
        public float val;
        public int lastCell = -1;

        public Line (int c1, int c2, int c3, float _val, int _lastCell = -1)
        {
            this.cells = new int[] { c1, c2, c3 };
            val = _val;
            lastCell = _lastCell;
        }

        int IComparable.CompareTo (Object obj)
        {
            Line secondLine = obj as Line;
            return this.val.CompareTo(secondLine.val);
        }
    }
}
