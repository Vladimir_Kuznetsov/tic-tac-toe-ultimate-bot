﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIGames.UltimateTicTacToe.StarterBot
{
    public class LimitedVal : ICloneable
    {
        public float val, min, max;
      
        public LimitedVal(float _min, float _max, float _val)
        {
            min = _min;
            max = _max;
            val = _val;
        }

        public void SetRandom()
        {
            val = Utils.RandomFloat(this.min, this.max);
        }

        public void ApplyGaussian()
        {
            double u1 = Utils.RandomFloat(0, 1);
            double u2 = Utils.RandomFloat(0, 1);
            double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                         Math.Sin(2.0 * Math.PI * u2);
            val = (float)(this.val + randStdNormal * (this.max - this.min) / 2);

            val = (val > max) ? max : (val < min) ? min : val;

            return;
        }

        public object Clone()
        {
            LimitedVal newVal = new LimitedVal(this.min, this.max, this.val);
            return newVal;
        }
    }
}
