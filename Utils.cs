﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIGames.UltimateTicTacToe.StarterBot.Communication;

namespace AIGames.UltimateTicTacToe.StarterBot
{
    static class Utils
    {

        private static Random _rnd;
        private static Random rnd
        {
            get
            {
                if (_rnd == null)
                    _rnd = new Random(System.DateTime.Now.Millisecond);
                return _rnd;
            }
        }

        /// <summary>
        /// Returns random float in range.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float RandomFloat (float min, float max)
        {
            return (float)(min + rnd.NextDouble() * (max - min));
        }

        /// <summary>
        /// Returns random int in range.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int RandomInt (int min, int max)
        {
            return rnd.Next(min, max);
        }

        /// <summary>Changes the player.</summary>
        public static byte SwitchPlayer(byte player)
        {
            if (player != 1 && player != 2)
                throw new ArgumentOutOfRangeException("player");

            return (byte) (2 / player);
        }

        /// <summary>Changes O to X and X to O.</summary>
        public static byte[] GetComplimentaryBoard(byte[] board)
        {
            byte[] complimentary = board.Clone() as byte[];
            for (int i = 0; i < complimentary.Length; i++)
            {
                if (complimentary[i] != 0)
                {
                    complimentary[i] = SwitchPlayer(complimentary[i]);
                }
            }
            return complimentary;
        }

        /// <summary>Cut tiny board out of meta board.</summary>
        public static byte[] GetTiny(byte[,] mb, int i)
        {
            byte[] resBoard = new byte[9];
            for (int k = 0; k < 9; k++)
                resBoard[k] = mb[(i / 3) * 3 + (k / 3), (i % 3) * 3 + (k % 3)];
            return resBoard;
        }

        /// <summary>Checks if two boards are equal.</summary>
        public static bool BoardsAreEqual(byte[] b1, byte[] b2)
        {
            if (ReferenceEquals(b1, b2))
                return true;

            if (b1 == null || b2 == null)
                return false;

            if (b1.Length != b2.Length)
                return false;
            
            for (int i = 0; i < b1.Length; i++)
            {
                if (b1[i] != b2[i])
                    return false;
            }
            return true;
        }

        /// <summary>Compares two boards for a move.</summary>
        public static int CompareBoards(TinyBoard b1, TinyBoard b2, int player, Settings s)
        {
            // We take into account value for the other player, because he cares about our victory more than we are.
            // Yes, I know it is stupid.
            float val1 = ((b1.tinyStrategy == TinyStrategy.winBoard ? 1.5f : 0) +
                        (b1.tinyStrategy == TinyStrategy.blockOpponent ? 1.3f : 0) +
                        (b1.tinyStrategy == TinyStrategy.basic || b1.tinyStrategy == TinyStrategy.dontCare ? 1f : 0)) *
                        TinyBoard.GetNotNormalizedTotalValue(b1, Utils.SwitchPlayer((byte)player), s);
            float val2 = ((b2.tinyStrategy == TinyStrategy.winBoard ? 1.5f : 0) +
                        (b2.tinyStrategy == TinyStrategy.blockOpponent ? 1.3f : 0) +
                        (b2.tinyStrategy == TinyStrategy.basic || b2.tinyStrategy == TinyStrategy.dontCare ? 1f : 0)) *
                        TinyBoard.GetNotNormalizedTotalValue(b2, Utils.SwitchPlayer((byte)player), s);
            return val1.CompareTo(val2);
        }

        /// <summary>Scales values so their sum equals to the given parameter.</summary>
        public static void NormalizeValues(ref List<float> weights, float newSum)
        {
            float oldSum = weights.Sum();
            if (oldSum > 0)
            {
                weights = weights.Select(x => x * newSum / oldSum).ToList();
            }
            else
            {
                float c = weights.Count;
                weights = weights.Select(x => 1 / c).ToList();
            }
        }

        /// <summary>Scales values so their sum equals to the given parameter.</summary>
        public static void NormalizeValues(ref float[] weights, float newSum)
        {
            List<float> l_weights = new List<float>(weights);
            NormalizeValues(ref l_weights, newSum);
            weights = l_weights.ToArray();
        }

        // Get a debug name of a tiny board strategy
        public static string GetStranegyName(TinyStrategy s)
        {
            switch (s)
            {
                case TinyStrategy.dontCare:
                    return "DC";
                case TinyStrategy.basic:
                    return "BS";
                case TinyStrategy.blockOpponent:
                    return "BO";
                case TinyStrategy.winBoard:
                    return "WB";
                default:
                    return "??";
            }
        }

        /// <summary>Clone each object in the array.</summary>
        public static T[] ArrayDeepCopy <T> (T[] orig) where T : class, ICloneable
        {
            return orig.Select(x => (T)x.Clone()).ToArray();
        }
    }
}
