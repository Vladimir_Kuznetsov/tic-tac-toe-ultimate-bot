﻿#if IS_DEBUG
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using AIGames.UltimateTicTacToe.StarterBot.Communication;
using AIGames.UltimateTicTacToe.StarterBot.Evolution;

namespace AIGames.UltimateTicTacToe.StarterBot
{
    public partial class DebugUI : Form
    {
        private enum DebugMode
        {
            Manual = 0,
            GeneticOff = 1,
            GeneticOn = 2
        }

        private DebugMode debugMode = DebugMode.Manual;

        private static byte[] board3x3;
        private static Button[] buttons3x3;
        private static Label[,] scoreLabels3x3;

        private static byte[,] board;
        private static Button[,] buttons;
        private static Label[,] labels;
        private static Panel[] panels;

        private static Color xCellColor = Color.Blue;
        private static Color oCellColor = Color.Red;
        private static Color xPanelColor = Color.LightBlue;
        private static Color oPanelColor = Color.LightPink;
        private static Color activeColor = Color.LightGray;

        private static MacroBoard macro;

        private static Starter bot;

        private static string logFile;

        private Thread autoPlayThread;

        public DebugUI()
        {
            InitializeComponent();
            InitializeGameElements();
            ResetLocalBoard();

            macro = new MacroBoard();

            var arg0 = new LimitedVal(0.1f, 0.4f, 0.18f);
            var arg1 = new LimitedVal(0.5f, 1.0f, 0.58f);
            var arg2 = new LimitedVal(1.0f, 3.0f, 1.53f);
            var arg3 = new LimitedVal(0.5f, 1.0f, 1.00f);
            var arg4 = new LimitedVal(1.0f, 3.0f, 1.00f);
            var arg5 = new LimitedVal(0.1f, 0.9f, 0.90f);
            var arg6 = new LimitedVal(0.1f, 0.8f, 0.19f);
            var arg7 = new LimitedVal(0.1f, 1.0f, 0.52f);
            var arg8 = new LimitedVal(0.9f, 2.0f, 0.90f);
            var arg9 = new LimitedVal(0.85f, 1.0f, 0.85f);
            var arg10 = new LimitedVal(0.5f, 3.0f, 0.50f);

            var args = new LimitedVal[] { arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10 };

            bot = new Starter(args);

            GameState game = new GameState();
            game.Macro = macro.Clone() as MacroBoard;
            foreach (var b in game.Macro.boards)
                b.isAvailable = true;
            bot.Game = game;

            Settings settings = new Settings();
            settings.YourBot = PlayerName.Player2;
            bot.ApplySettings(settings);
        }

        private void InitializeGameElements()
        {
            board3x3 = new byte[9];
            board = new byte[9, 9];
            macro = new MacroBoard();
            buttons3x3 = new Button[] { btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8};
            scoreLabels3x3 = new Label[,] 
            { 
                { lab0, lab1, lab2, lab3, lab4, lab5, lab6, lab7, lab8 } ,
                { lab9, lab10, lab11, lab12, lab13, lab14, lab15, lab16, lab17 }
            };
            buttons = new Button[,]
            {
                { btn0_0, btn0_1, btn0_2, btn1_0, btn1_1, btn1_2, btn2_0, btn2_1, btn2_2 },
                { btn0_3, btn0_4, btn0_5, btn1_3, btn1_4, btn1_5, btn2_3, btn2_4, btn2_5 },
                { btn0_6, btn0_7, btn0_8, btn1_6, btn1_7, btn1_8, btn2_6, btn2_7, btn2_8 },
                { btn3_0, btn3_1, btn3_2, btn4_0, btn4_1, btn4_2, btn5_0, btn5_1, btn5_2 },
                { btn3_3, btn3_4, btn3_5, btn4_3, btn4_4, btn4_5, btn5_3, btn5_4, btn5_5 },
                { btn3_6, btn3_7, btn3_8, btn4_6, btn4_7, btn4_8, btn5_6, btn5_7, btn5_8 },
                { btn6_0, btn6_1, btn6_2, btn7_0, btn7_1, btn7_2, btn8_0, btn8_1, btn8_2 },
                { btn6_3, btn6_4, btn6_5, btn7_3, btn7_4, btn7_5, btn8_3, btn8_4, btn8_5 },
                { btn6_6, btn6_7, btn6_8, btn7_6, btn7_7, btn7_8, btn8_6, btn8_7, btn8_8 }
            };
            labels = new Label[,]
            {
                { lab0_0, lab0_1, lab0_2, lab0_3, lab0_4, lab0_5, lab0_6, lab0_7, lab0_8 },
                { lab1_0, lab1_1, lab1_2, lab1_3, lab1_4, lab1_5, lab1_6, lab1_7, lab1_8 }
            };
            panels = new Panel[] { pnlBoard0, pnlBoard1, pnlBoard2, pnlBoard3, pnlBoard4, pnlBoard5, pnlBoard6, pnlBoard7, pnlBoard8};
        }

        private void ResetLocalBoard()
        {
            for (int i = 0; i < board3x3.Length; i++)
                board3x3[i] = 0;
            UpdateLocalBoard();
        }

        private char ConvertToASCI(int val)
        {
            switch (val)
            {
                case 1:
                    return 'X';
                case 2:
                    return 'O';
                default:
                    return ' ';
            }
        }

        private void btn_Click_3x3(int i)
        {
            UpdateLocalBoard(i, (byte)((board3x3[i] + 1) % 3));
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            btn_Click_3x3(0);
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            btn_Click_3x3(1);
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            btn_Click_3x3(2);
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            btn_Click_3x3(3);
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            btn_Click_3x3(4);
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            btn_Click_3x3(5);
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            btn_Click_3x3(6);
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            btn_Click_3x3(7);
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            btn_Click_3x3(8);
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            ResetLocalBoard();
            UpdateLocalBoard();
        }

        private void BtnGo_Click(object sender, EventArgs e)
        {
            System.DateTime startTime = System.DateTime.Now;

            TinyBoard game = TinyBoard.New(board3x3);
            game.tinyStrategy = TinyStrategy.blockOpponent;
            int winner = game.GetWinner();

            switch (winner)
            {
                case 0:
                    labStatus.Text = "game tie";
                    break;
                case 1:
                case 2:
                    labStatus.Text = "player " + winner + " won the game";
                    break;
                case -1:
                    MacroBoard macro = new MacroBoard();

                    game.boardNum = (int) nudNumber.Value;
                    float[] weights = new float[9];
                    float.TryParse(tbWeight0.Text.Replace('.', ','), out weights[0]);
                    float.TryParse(tbWeight1.Text.Replace('.', ','), out weights[1]);
                    float.TryParse(tbWeight2.Text.Replace('.', ','), out weights[2]);
                    float.TryParse(tbWeight3.Text.Replace('.', ','), out weights[3]);
                    float.TryParse(tbWeight4.Text.Replace('.', ','), out weights[4]);
                    float.TryParse(tbWeight5.Text.Replace('.', ','), out weights[5]);
                    float.TryParse(tbWeight6.Text.Replace('.', ','), out weights[6]);
                    float.TryParse(tbWeight7.Text.Replace('.', ','), out weights[7]);
                    float.TryParse(tbWeight8.Text.Replace('.', ','), out weights[8]);

                    for (int k = 0; k < 9; k++)
                        macro.boards[k].totalValue = weights[k];

                    game.totalValue = weights[game.boardNum];

                    var scores1 = game.GetScoreTable(1, macro, bot.Settings);
                    var scores2 = game.GetScoreTable(2, macro, bot.Settings);
                    UpdateLocalScores(scores1, scores2);

                    game.UpdateValues(new MacroBoard(), bot.Settings);
                    var valueForPlayer1 = game.valueForPlayer1;
                    var valueForPlayer2 = game.valueForPlayer2;

                    labStatus.Text = "game in progress, player1 = " + valueForPlayer1 + ", player2 = " + valueForPlayer2 + ", totalValue = " + TinyBoard.GetNotNormalizedTotalValue(game, 0, bot.Settings);
                    break;
            }
            Console.WriteLine("Time spent: " + (startTime - System.DateTime.Now).TotalMilliseconds);
        }

        private void UpdateLocalScores(float[] scores1, float[] scores2)
        {
            for (int i = 0; i < 9; i++)
            {
                scoreLabels3x3[0, i].Text = scores1[i].ToString();
                scoreLabels3x3[1, i].Text = scores2[i].ToString();
            }
        }

        private void UpdateLocalBoard(int index = -1, byte val = 0)
        {
            if (index >= 0)
            {
                board3x3[index] = val;
            }
            for (int i = 0; i < 9; i++)
            {
                buttons3x3[i].Text = ConvertToASCI(board3x3[i]).ToString();
            }
        }

        private void PrintBoard(byte[] data)
        {
            int[] i_data = new int[data.Length];
            for (int i = 0; i < data.Length; i++)
                i_data[i] = (int)data[i];
            PrintBoard(i_data);
        }

        private void UpdateWeights()
        {
            macro.SetMetaBoard(board);
            MacroBoard bufferMacro = macro.Clone() as MacroBoard;
            for (int i = 0; i < 9; i++)
            {
                macro.boards[i].UpdateValues(bufferMacro, bot.Settings);
            }
            Strategy.DoStrategy(macro, 2, bot.Settings);
            for (int i = 0; i < 9; i++)
            {
                macro.boards[i].UpdateValues(bufferMacro, bot.Settings);
            }
            macro.UpdateWeights(1, bot.Settings);
        }

        private BotResponse MakeMove_Ultimate(IBot b, byte player)
        {
            var response = b.GetResponse();
            int x = response.Move.Move.X;
            int y = response.Move.Move.Y;

            return response;
        }

        private void btn_Click_Ultimate(int y, int x)
        {
            if (debugMode == DebugMode.Manual)
            {
                if (y >= 0 && x >= 0)
                {
                    Stopwatch timer = new Stopwatch();

                    if (macro.GetCell(x, y) != 0)
                        return;

                    timer.Start();

                    UpdateMacroForGame(x, y, 1);
                    UpdateMacroForBot(bot);
                    var resp = MakeMove_Ultimate(bot, 2);
                    UpdateMacroForGame(resp.Move.Move.X, resp.Move.Move.Y, 2);

                    for (int h = 0; h < 9; h++)
                        labels[0, h].Text = bot.Game.Macro.boards[h].totalValue.ToString();

                    for (int k = 0; k < 9; k++)
                        labels[1, k].Text = bot.Game.Macro.boards[k].macroStrategy.ToString();

                    int winner = getMacroWinner();
                    if (winner != -1)
                        Console.WriteLine(winner == 0 ? "The game is tie." : "Player {0} won the game.", winner);

                    Console.WriteLine("Move time: " + timer.Elapsed.TotalMilliseconds);
                }
            }
        }

        private void UpdateMacroForGame(int x, int y, byte player)
        {

            try
            {
                // Display player move with symbol and color
                if (x >= 0 && y >= 0)
                {
                    board[y, x] = player;
                        buttons[y, x].Invoke(new MethodInvoker(() =>
                        {
                            buttons[y, x].Text = ConvertToASCI(board[y, x]) + "";
                            buttons[y, x].ForeColor = (board[y, x] == 1) ? xCellColor : oCellColor;
                        }));
                    macro.SetMetaBoard(board);
            
                // Clear all cells
                } else
                {
                    foreach (var b in buttons)
                        b.Invoke(new MethodInvoker(() =>
                        {
                            b.ForeColor = Color.FromKnownColor(KnownColor.ControlText);
                            b.BackColor = Color.Transparent;
                        }));
                }

                // Color finished game boards
                for (int i = 0; i < 9; i++)
                {
                    int winner = macro.boards[i].GetWinner();
                    panels[i].Invoke(new MethodInvoker(() =>
                    {
                        switch (winner)
                        {
                            case 1:
                                panels[i].BackColor = xPanelColor;
                                break;
                            case 2:
                                panels[i].BackColor = oPanelColor;
                                break;
                            default:
                                panels[i].BackColor = Color.Transparent;
                                break;
                        }
                    }));
                }

                // Color next board(s)
                int nextBoard = x % 3 + (y % 3) * 3;
                foreach (var b in macro.boards)
                    b.isAvailable = false;
                if (x >= 0 && y >= 0 && macro.boards[nextBoard].GetWinner() == -1)
                {
                    macro.boards[nextBoard].isAvailable = true;
                    panels[nextBoard].Invoke(new MethodInvoker(() => panels[nextBoard].BackColor = activeColor));
                }
                else
                    for (int i = 0; i < 9; i++)
                    {
                        if (macro.boards[i].GetWinner() == -1)
                        {
                            macro.boards[i].isAvailable = true;
                            panels[i].Invoke(new MethodInvoker(() => panels[i].BackColor = activeColor));
                        }
                        }
            }
            catch (Exception e) { }
        }

        private void UpdateMacroForBot (IBot b)
        {
            b.Game.Apply(MakeMacroboardInstruction());
            b.Game.Apply(MakeFieldInstruction());
        }


        private IInstruction MakeMacroboardInstruction ()
        {
            string s_instruction = "update game macroboard ";
            s_instruction += String.Concat(macro.boards.Select<TinyBoard, string>(x => x.isAvailable ? "-1," : "0,"));
            s_instruction = s_instruction.TrimEnd(',');
            return Instruction.Parse(s_instruction);
        }

        private IInstruction MakeFieldInstruction()
        {
            string s_instruction = "update game field ";
            for (int x = 0; x < 9; x++)
                for (int y = 0; y < 9; y++)
                    s_instruction += board[x, y].ToString() + ",";
            s_instruction = s_instruction.TrimEnd(',');
            return Instruction.Parse(s_instruction);
        }

        private int getMacroWinner()
        {
            int winner = 0;

            // Check for empty cells
            for (int i = 0; i < 9; i++)
                if (macro.boards[i].GetWinner() == -1)
                {
                    winner = -1;
                    break;
                }

            // Check rows
            for (int i = 0; i < 3; i++)
                if ((macro.boards[i * 3].GetWinner() > 0) && (macro.boards[i * 3].GetWinner() == macro.boards[i * 3 + 1].GetWinner()) &&
                                            (macro.boards[i * 3 + 1].GetWinner() == macro.boards[i * 3 + 2].GetWinner()))
                {
                    winner = macro.boards[i * 3].GetWinner();
                    break;
                }

            // Check columns
            for (int j = 0; j < 3; j++)
                if ((macro.boards[j].GetWinner() > 0) && (macro.boards[j].GetWinner() == macro.boards[j + 3].GetWinner()) &&
                                           (macro.boards[j + 3].GetWinner() == macro.boards[j + 6].GetWinner()))
                {
                    winner = macro.boards[j].GetWinner();
                    break;
                }

            // Check diagonals
            if ((macro.boards[4].GetWinner() > 0) && ((macro.boards[0].GetWinner() == macro.boards[4].GetWinner() && macro.boards[4].GetWinner() == macro.boards[8].GetWinner()) ||
                                    (macro.boards[4].GetWinner() == macro.boards[2].GetWinner() && macro.boards[4].GetWinner() == macro.boards[6].GetWinner())))
                winner = macro.boards[4].GetWinner();

            return winner;
        }

        private void btn0_0_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(0, 0);
        }

        private void PrintBoard (int[] data)
        {
            Console.WriteLine(data[0] + " " + data[1] + " " + data[2]);
            Console.WriteLine(data[3] + " " + data[4] + " " + data[5]);
            Console.WriteLine(data[6] + " " + data[7] + " " + data[8]);
        }

        private void btn0_1_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(0, 1);
        }

        private void btn0_2_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(0, 2);
        }

        private void btn1_0_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(0, 3);
        }

        private void btn1_1_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(0, 4);
        }

        private void btn1_2_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(0, 5);
        }

        private void btn2_0_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(0, 6);
        }

        private void btn2_1_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(0, 7);
        }

        private void btn2_2_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(0, 8);
        }

        private void btn0_3_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(1, 0);
        }

        private void btn0_4_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(1, 1);
        }

        private void btn0_5_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(1, 2);
        }

        private void btn1_3_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(1, 3);
        }

        private void btn1_4_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(1, 4);
        }

        private void btn1_5_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(1, 5);
        }

        private void btn2_3_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(1, 6);
        }

        private void btn2_4_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(1, 7);
        }

        private void btn2_5_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(1, 8);
        }

        private void btn0_6_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(2, 0);
        }

        private void btn0_7_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(2, 1);
        }

        private void btn0_8_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(2, 2);
        }

        private void btn1_6_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(2, 3);
        }

        private void btn1_7_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(2, 4);
        }

        private void btn1_8_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(2, 5);
        }

        private void btn2_6_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(2, 6);
        }

        private void btn2_7_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(2, 7);
        }

        private void btn2_8_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(2, 8);
        }

        private void btn3_0_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(3, 0);
        }

        private void btn3_1_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(3, 1);
        }

        private void btn3_2_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(3, 2);
        }

        private void btn4_0_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(3, 3);
        }

        private void btn4_1_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(3, 4);
        }

        private void btn4_2_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(3, 5);
        }

        private void btn5_0_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(3, 6);
        }

        private void btn5_1_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(3, 7);
        }

        private void btn5_2_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(3, 8);
        }

        private void btn3_3_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(4, 0);
        }

        private void btn3_4_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(4, 1);
        }

        private void btn3_5_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(4, 2);
        }

        private void btn4_3_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(4, 3);
        }

        private void btn4_4_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(4, 4);
        }

        private void btn4_5_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(4, 5);
        }

        private void btn5_3_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(4, 6);
        }

        private void btn5_4_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(4, 7);
        }

        private void btn5_5_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(4, 8);
        }

        private void btn3_6_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(5, 0);
        }

        private void btn3_7_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(5, 1);
        }

        private void btn3_8_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(5, 2);
        }

        private void btn4_6_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(5, 3);
        }

        private void btn4_7_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(5, 4);
        }

        private void btn4_8_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(5, 5);
        }

        private void btn5_6_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(5, 6);
        }

        private void btn5_7_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(5, 7);
        }

        private void btn5_8_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(5, 8);
        }

        private void btn6_0_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(6, 0);
        }

        private void btn6_1_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(6, 1);
        }

        private void btn6_2_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(6, 2);
        }

        private void btn7_0_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(6, 3);
        }

        private void btn7_1_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(6, 4);
        }

        private void btn7_2_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(6, 5);
        }

        private void btn8_0_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(6, 6);
        }

        private void btn8_1_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(6, 7);
        }

        private void btn8_2_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(6, 8);
        }

        private void btn6_3_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(7, 0);
        }

        private void btn6_4_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(7, 1);
        }

        private void btn6_5_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(7, 2);
        }

        private void btn7_3_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(7, 3);
        }

        private void btn7_4_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(7, 4);
        }

        private void btn7_5_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(7, 5);
        }

        private void btn8_3_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(7, 6);
        }

        private void btn8_4_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(7, 7);
        }

        private void btn8_5_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(7, 8);
        }

        private void btn6_6_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(8, 0);
        }

        private void btn6_7_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(8, 1);
        }

        private void btn6_8_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(8, 2);
        }

        private void btn7_6_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(8, 3);
        }

        private void btn7_7_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(8, 4);
        }

        private void btn7_8_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(8, 5);
        }

        private void btn8_6_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(8, 6);
        }

        private void btn8_7_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(8, 7);
        }

        private void btn8_8_Click(object sender, EventArgs e)
        {
            btn_Click_Ultimate(8, 8);
        }

        private void GeneticBtn_Click(object sender, EventArgs e)
        {
            if (debugMode == DebugMode.GeneticOn)
            {
                StopGenetic();
            }
            else
            {
                StartGenetic();
            }
        }

        private void StartGenetic()
        {
            txtLog.Visible = true;
            txtLog.Text = "";
            labStrategy.Visible = false;
            labTotal.Visible = false;
            pnlStrategy.Visible = false;
            pnlTotal.Visible = false;

            debugMode = DebugMode.GeneticOn;
            GeneticBtn.Text = "Stop Genetic";

            autoPlayThread = new Thread(() => DoEvolution());
            autoPlayThread.Start();
        }

        private void StopGenetic()
        {
            if (autoPlayThread != null && autoPlayThread.IsAlive)
                autoPlayThread.Abort();

            debugMode = DebugMode.GeneticOff;
            GeneticBtn.Text = "Start Genetic";
        }

        private void StartManual()
        {
            txtLog.Visible = false;
            labStrategy.Visible = true;
            labTotal.Visible = true;
            pnlStrategy.Visible = true;
            pnlTotal.Visible = true;

            StopGenetic();

            debugMode = DebugMode.Manual;
            ClearBoard();
        }

        private void ClearBoard()
        {
            for (int i = 0; i < board.GetLength(0); i++)
                for (int j = 0; j < board.GetLength(1); j++)
                    board[i, j] = 0;
            macro.SetMetaBoard(board);
            foreach (var b in buttons)
                b.Invoke((MethodInvoker)(() => b.Text = " "));
            UpdateMacroForGame(-1, -1, 0);
        }

        private void CreateLogFile()
        { 
            string logDirectory = @Path.GetDirectoryName(Application.ExecutablePath) + @"\logs\";
            logFile = logDirectory + "log_" + (DateTime.Now.ToShortDateString() + "_" + DateTime.Now.ToShortTimeString()).Replace(":", "").Replace(".", "") + ".txt";

            if (!Directory.Exists(logDirectory))
                Directory.CreateDirectory(logDirectory);

            if (File.Exists(logFile)) return;

            File.Create(logFile).Dispose();
        }
        
        private void LogData(string data)
        {
            txtLog.Invoke(new MethodInvoker(() => txtLog.AppendText(data + System.Environment.NewLine)));

            if (logFile == null || logFile == "" || !Directory.Exists(@Path.GetDirectoryName(Application.ExecutablePath) + @"\logs\") || !File.Exists(logFile))
                CreateLogFile();
            
            using (StreamWriter file = new StreamWriter(logFile, true))
            {
                file.WriteLine(data);
            }
        }

        private void DoEvolution()
        {

            // Create base genes
            var arg0 = new LimitedVal(0.1f, 0.4f, 0.13f);
            var arg1 = new LimitedVal(0.5f, 1.0f, 0.50f);
            var arg2 = new LimitedVal(1.0f, 3.0f, 1.00f);
            var arg3 = new LimitedVal(0.5f, 1.0f, 0.59f);
            var arg4 = new LimitedVal(1.0f, 3.0f, 1.00f);
            var arg5 = new LimitedVal(0.1f, 0.9f, 0.17f);
            var arg6 = new LimitedVal(0.1f, 0.8f, 0.13f);
            var arg7 = new LimitedVal(0.1f, 1.0f, 0.19f);
            var arg8 = new LimitedVal(0.9f, 2.0f, 1.28f);
            var arg9 = new LimitedVal(0.85f, 1.0f, 0.91f);
            var arg10 = new LimitedVal(0.5f, 3.0f, 2f);

            var args = new LimitedVal[] { arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10 };

            // Initialize population
            Population botPopulation = new Population(args);
            botPopulation.InitRandom(50);

            botPopulation.creatures[1].SetName("[ 0,14 0,96 2,98 0,62 1,71 0,12 0,50 0,93 1,78 0,88 0,67 ]");
            botPopulation.creatures[2].SetName("[ 0,31 0,60 1,57 0,91 2,97 0,72 0,10 0,52 0,97 0,93 0,50 ]");
            botPopulation.creatures[3].SetName("[ 0,27 0,60 1,57 1,00 1,63 0,72 0,10 0,52 0,97 0,93 0,52 ]");
            botPopulation.creatures[4].SetName("[ 0,31 0,60 1,26 0,91 2,97 0,72 0,23 0,52 0,97 0,97 0,50 ]");
            botPopulation.creatures[5].SetName("[ 0,27 0,60 2,06 1,00 2,97 0,72 0,10 0,52 0,97 0,93 0,50 ]");
            botPopulation.creatures[6].SetName("[ 0,31 0,53 1,58 1,00 3,00 0,90 0,10 0,52 0,90 0,93 0,73 ]");
            botPopulation.creatures[7].SetName("[ 0,17 0,50 1,17 1,00 2,14 0,54 0,10 0,59 0,90 1,00 0,69 ]");
            botPopulation.creatures[8].SetName("[ 0,23 0,71 1,35 0,50 1,30 0,17 0,17 0,75 0,93 1,00 0,85 ]");
            botPopulation.creatures[9].SetName("[ 0,23 0,71 1,35 0,50 1,30 0,10 0,17 0,75 0,93 1,00 0,85 ]");
            botPopulation.creatures[10].SetName("[ 0,17 0,50 1,00 0,98 2,14 0,54 0,10 1,00 0,94 0,90 0,69 ]");
            botPopulation.creatures[11].SetName("[ 0,40 0,50 2,88 0,80 2,46 0,41 0,71 0,10 1,67 0,85 0,50 ]");
            botPopulation.creatures[12].SetName("[ 0,24 0,50 2,88 0,80 2,46 0,41 0,57 0,10 1,67 0,85 0,50 ]");
            botPopulation.creatures[13].SetName("[ 0,24 0,50 2,88 1,00 2,46 0,41 0,57 0,10 1,67 0,85 0,50 ]");
            botPopulation.creatures[14].SetName("[ 0,31 0,50 1,26 0,91 2,97 0,72 0,23 0,38 0,97 0,97 0,50 ]");
            botPopulation.creatures[15].SetName("[ 0,24 0,50 2,88 1,00 2,46 0,41 0,57 0,10 1,67 1,00 0,50 ]");
            botPopulation.creatures[16].SetName("[ 0,10 0,73 1,24 1,00 2,59 0,53 0,29 0,52 0,90 0,95 0,50 ]");
            botPopulation.creatures[17].SetName("[ 0,31 0,53 1,58 1,00 3,00 0,90 0,10 0,52 1,08 0,93 0,73 ]");
            botPopulation.creatures[18].SetName("[ 0,10 0,52 1,00 1,00 1,00 0,90 0,19 0,52 0,90 0,86 0,50 ]");
            botPopulation.creatures[19].SetName("[ 0,40 0,53 1,58 1,00 3,00 0,90 0,10 0,52 1,24 0,93 0,73 ]");
            botPopulation.creatures[20].SetName("[ 0,40 0,50 2,88 0,80 1,00 0,41 0,71 0,10 1,67 0,85 0,50 ]");
            botPopulation.creatures[21].SetName("[ 0,24 0,50 2,88 1,00 2,46 0,10 0,57 0,10 1,67 0,85 0,50 ]");
            botPopulation.creatures[22].SetName("[ 0,40 0,60 1,45 0,50 2,82 0,53 0,10 0,30 1,17 1,00 0,50 ]");
            botPopulation.creatures[23].SetName("[ 0,40 0,66 1,00 1,00 1,00 0,90 0,10 0,52 0,90 0,85 0,50 ]");
            botPopulation.creatures[24].SetName("[ 0,27 0,50 1,25 0,53 1,00 0,15 0,36 0,72 0,91 1,00 0,93 ]");
            botPopulation.creatures[25].SetName("[ 0,40 0,66 1,00 1,00 2,59 0,49 0,10 0,52 0,90 0,85 0,50 ]");
            botPopulation.creatures[26].SetName("[ 0,27 0,50 1,25 0,53 1,00 0,10 0,36 0,72 0,91 1,00 0,93 ]");
            botPopulation.creatures[27].SetName("[ 0,10 0,52 1,53 1,00 1,00 0,90 0,19 0,52 0,90 0,96 0,50 ]");
            botPopulation.creatures[28].SetName("[ 0,40 0,66 1,45 0,87 2,82 0,53 0,10 0,30 1,17 1,00 0,50 ]");
            botPopulation.creatures[29].SetName("[ 0,10 0,52 1,53 1,00 1,00 0,90 0,19 0,52 0,90 0,93 0,50 ]");
            botPopulation.creatures[30].SetName("[ 0,10 0,50 1,53 1,00 1,00 0,90 0,19 0,52 0,90 0,92 0,50 ]");
            botPopulation.creatures[31].SetName("[ 0,33 0,50 1,25 0,66 1,00 0,10 0,22 0,96 0,90 0,85 0,50 ]");
            botPopulation.creatures[32].SetName("[ 0,18 0,58 1,53 1,00 1,00 0,90 0,19 0,52 0,90 0,85 0,50 ]");
            botPopulation.creatures[33].SetName("[ 0,10 0,50 1,00 1,00 1,00 0,90 0,19 0,66 0,90 0,96 0,50 ]");
            botPopulation.creatures[34].SetName("[ 0,18 0,50 1,00 1,00 1,30 0,54 0,33 0,31 1,24 0,96 0,69 ]");
            botPopulation.creatures[35].SetName("[ 0,24 0,50 2,88 1,00 2,46 0,41 0,57 0,10 1,67 0,87 0,50 ]");
            botPopulation.creatures[36].SetName("[ 0,10 0,52 1,53 1,00 1,07 0,90 0,19 0,52 0,90 0,95 0,55 ]");
            botPopulation.creatures[37].SetName("[ 0,24 0,50 2,88 1,00 2,46 0,31 0,57 0,10 1,67 1,00 0,50 ]");
            botPopulation.creatures[38].SetName("[ 0,10 0,50 1,00 0,78 1,00 0,90 0,19 0,66 0,90 0,96 0,50 ]");
            botPopulation.creatures[39].SetName("[ 0,10 0,50 1,53 0,92 1,00 0,72 0,10 0,52 0,90 0,97 0,55 ]");
            botPopulation.creatures[40].SetName("[ 0,31 0,60 1,57 0,93 2,97 0,41 0,10 0,52 0,93 0,93 0,50 ]");
        
            LogData("Random population created");
            LogData("Starting evolution");
            foreach (var c in botPopulation.creatures)
                LogData(c.bot.GetName());
            SendMail();

            Random rnd = new Random((int)(System.DateTime.Now.Ticks % int.MaxValue));

            int generation = 0;
            while (true)
            {
                // Bots fight against each other
                for (int i = 0; i < botPopulation.creatures.Count; i++)
                    for (int j = 0; j < botPopulation.creatures.Count; j++)
                    {
                        if (i == j)
                            continue;
                        
                        int winner = GetFightResult(botPopulation.creatures[i].bot, botPopulation.creatures[j].bot);
                        switch (winner)
                        {
                            case 0:
                                botPopulation.creatures[i].points += 2f;
                                botPopulation.creatures[j].points += 2f;
                                break;
                            case 1:
                                botPopulation.creatures[i].points += 3f;
                                break;
                            case 2:
                                botPopulation.creatures[j].points += 3.5f;
                                break;
                        }

                        // Display fight result
                        Thread.Sleep(500);
                    }

                // Display generation results
                botPopulation.creatures.Sort((c1, c2) => c2.points.CompareTo(c1.points));
                LogData("-----");
                LogData("Generation #" + generation + ":");
                foreach (var c in botPopulation.creatures)
                    LogData(c.bot.GetName() + " => " + c.points);

                // Every 5 generations, report favorits
                if (generation % 5 == 0 && generation > 0)
                {
                    LogData("-----");
                    LogData("Best fighters:");
                    foreach(var c in botPopulation.bestFighters)
                        LogData(c.bot.GetName());
                }

                // Every generation, send email report
                if (generation % 1 == 0 && generation > 0)
                {
                    SendMail();
                }

                // Next generation
                botPopulation.NextGeneration();
                generation++;
            }
        }

        private int GetFightResult(IBot p1, IBot p2)
        {
            // Clear the board before new fight
            ClearBoard();

            // Initialize both bots
            IBot[] bots = new IBot[] { p1, p2 };
            GameState game = new GameState();
            game.Macro = macro.Clone() as MacroBoard;
            foreach (var b in game.Macro.boards)
                b.isAvailable = true;
            bots[0].Game = game;
            bots[1].Game = game;

            Settings settings1 = new Settings();
            settings1.YourBot = PlayerName.Player1;
            bots[0].ApplySettings(settings1);
            Settings settings2 = new Settings();
            settings2.YourBot = PlayerName.Player2;
            bots[1].ApplySettings(settings2);

            // Fight till the end
            int moveNumber = 0;
            do
            {
                var botNumber = moveNumber % 2;
                var oppoNumber = (moveNumber + 1) % 2;
                var resp = MakeMove_Ultimate(bots[botNumber], (byte)(botNumber + 1));
                UpdateMacroForGame(resp.Move.Move.X, resp.Move.Move.Y, (byte) (botNumber + 1));
                UpdateMacroForBot(bots[oppoNumber]);
                moveNumber++;
                Thread.Sleep(1);

            } while (getMacroWinner() == -1);

            return getMacroWinner();
        }

        private void ManualBtn_Click(object sender, EventArgs e)
        {
            StartManual();
        }

        /// <summary>
        /// Send an email report.
        /// </summary>
        /// <param name="smtpServer">Имя SMTP-сервера</param>
        /// <param name="from">Адрес отправителя</param>
        /// <param name="password">пароль к почтовому ящику отправителя</param>
        /// <param name="mailto">Адрес получателя</param>
        /// <param name="caption">Тема письма</param>
        /// <param name="message">Сообщение</param>
        /// <param name="attachFile">Присоединенный файл</param>
        public static void SendMail(string smtpServer, string from, string password,
        string mailto, string caption, string message, string attachFile = null)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);
                mail.To.Add(new MailAddress(mailto));
                mail.Subject = caption;
                mail.Body = message;
                if (!string.IsNullOrEmpty(attachFile))
                    mail.Attachments.Add(new Attachment(attachFile));
                SmtpClient client = new SmtpClient();
                client.Host = smtpServer;
                client.Port = 587;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(from.Split('@')[0], password);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);
                mail.Dispose();
            }
            catch (Exception e)
            {
                throw new Exception("Mail.Send: " + e.Message);
            }
        }

        /// <summary>
        /// Send an email report.
        /// </summary>
        private void SendMail()
        {
            try
            {
                SendMail("smtp.gmail.com", "tictactoe.evolution@gmail.com", "qwerty123qwerty", "kzntsv.vldmr@gmail.com", "TTT evolution log data", " ", logFile);
            } catch (Exception e)
            {
                Console.Error.WriteLine("Problem while sending email report.");
            }
        }

        /// <summary>
        /// Hide me!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void hideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
#endif