﻿using AIGames.UltimateTicTacToe.StarterBot.Communication;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace AIGames.UltimateTicTacToe.StarterBot
{
	public class Starter : IBot
	{
        public LimitedVal[] args;

		public Starter(LimitedVal[] _args)
		{
			Rnd = new Random();
            args = _args;
        }

		public Settings Settings { get; set; }
		public GameState Game { get; set; }
		private Random Rnd { get; set; }

        /// <summary>Copy bot by value.</summary>
        public object Clone()
        {
            Starter newBot = new Starter(Utils.ArrayDeepCopy<LimitedVal>(this.args));
            if (this.Settings != null)
            {
                newBot.Settings = new Settings();
                newBot.Settings.YourBot = this.Settings.YourBot;
                newBot.UseArgs();
            }
            if (this.Game != null)
            {
                newBot.Game = this.Game.Clone() as GameState;
            }

            return newBot;
        }

        /// <summary>Convert args to a string name.</summary>
        public string GetName()
        {
            if (args == null)
                return "no name";
            string name = "[ ";
            for (int i = 0; i < args.Length; i++)
                name += (args[i].val.ToString("F") + " ");
            name += "]";
            return name;
        }

        /// <summary>Set constants.</summary>
        public void UseArgs()
        {
            if (Settings != null)
                this.Settings.Configure(args);
        }
        
        /// <summary>Apply settings on start.</summary>
		public void ApplySettings(Settings _settings)
		{
			this.Settings = _settings;
            this.UseArgs();
		}

        /// <summary>Updates game state every move.</summary>
		public void Update(GameState _game)
		{
			Game = _game;
		}

        /// <summary>Update board values in background.</summary>
        public IEnumerable DoInBackground()
        {
            yield return null;
        }

        /// <summaryUpdate board values and strategy.</summary>
        private void UpdateBoards()
        {
            // Update board values
            if (Game != null && Settings != null)
            {
                MacroBoard bufferMacro = Game.Macro.Clone() as MacroBoard;
                foreach (TinyBoard b in Game.Macro.boards)
                    b.UpdateValues(bufferMacro, this.Settings);

                // The evil planning goes here
                Strategy.DoStrategy(Game.Macro, (byte)Settings.YourBot, this.Settings);

                // Correct our values according to the plan
                bufferMacro = Game.Macro.Clone() as MacroBoard;
                foreach (TinyBoard b in Game.Macro.boards)
                    b.UpdateValues(bufferMacro, this.Settings);
                Game.Macro.UpdateWeights((int)Settings.OppoBot, this.Settings);
            }
        }

        /// <summary>Make a move.</summary>
		public BotResponse GetResponse()
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();

            UpdateBoards();

            // Get a full list of all possible moves and scores
            float[] bestScoreInBoard = new float[9];
            for (int k = 0; k < 9; k++)
                if (Game.Macro.boards[k].isAvailable)
                    bestScoreInBoard[k] = Game.Macro.boards[k].GetScoreTable((byte)Settings.YourBot, Game.Macro, this.Settings).Max()
                        * (Game.Macro.boards[k].tinyStrategy == TinyStrategy.blockOpponent ? 1.7f :
                        Game.Macro.boards[k].tinyStrategy == TinyStrategy.winBoard ? 1.4f : 1f)
                        / (float) Math.Pow(Game.Macro.boards[k].totalValue, 2);

            // Choose a tiny board to make the next move
            int tiny = -1;
            for (int k = 0; k < 9; k++)
            {
                if (Game.Macro.boards[k].isAvailable && (tiny < 0 || bestScoreInBoard[k] > bestScoreInBoard[tiny]))
                    tiny = k;
            }

            float[] scores = Game.Macro.boards[tiny].GetScoreTable((byte)Settings.YourBot, Game.Macro, this.Settings);
            int k_max = -1;

            for (int k = 0; k < 9; k++)
            {
                if ((k_max == -1 || scores[k] > scores[k_max]) && Game.Macro.boards[tiny].board[k] == 0)
                    k_max = k;
            }

            int x = (tiny % 3) * 3 + k_max % 3;
            int y = (tiny / 3) * 3 + k_max / 3;

            Game.Macro.SetCell((byte)Settings.YourBot, x, y);

            Move move = new Move(x, y);
            timer.Stop();

            string log = "Round #" + Game.Round + ", time: " + timer.Elapsed.TotalMilliseconds + System.Environment.NewLine;
            log += "Strategy vals:" + System.Environment.NewLine;
            for (int i = 0; i < 9; i++)
                log += Game.Macro.boards[i].macroStrategy + " " + (i % 3 == 2 ? System.Environment.NewLine : "");

            log += "Tiny strategies:" + System.Environment.NewLine;
            for (int i = 0; i < 9; i++)
                log += Utils.GetStranegyName(Game.Macro.boards[i].tinyStrategy) + " " + (i % 3 == 2 ? System.Environment.NewLine : "");

            log += "Total vals:" + System.Environment.NewLine;
            for (int i = 0; i < 9; i++)
                log += Game.Macro.boards[i].totalValue + " " + (i % 3 == 2 ? System.Environment.NewLine : "");


            var r = new BotResponse()
            {
                Move = new MoveInstruction(move),
                Log = log,
			};

			return r;
		}
    }
}
