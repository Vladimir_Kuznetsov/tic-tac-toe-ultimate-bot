﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIGames.UltimateTicTacToe.StarterBot.Communication;

namespace AIGames.UltimateTicTacToe.StarterBot
{

    public enum TinyStrategy
    {
        basic,
        blockOpponent,
        winBoard,
        dontCare,
    }

    public class TinyBoard : ICloneable
    {
        // Data required for score calculations
        public struct MinimaxContext
        {
            public byte player, depth;
            public bool justSkipped;
            public MacroBoard macro;

            public MinimaxContext(byte _player, byte _depth, MacroBoard _macro, bool _justSkipped)
            {
                player = _player;
                depth = _depth;
                justSkipped = _justSkipped;
                macro = _macro;
            }
        }

        public byte[] board = new byte[9];
        public int boardNum = 0;
        public int winner = -1;
        public bool canWin1 = true, canWin2 = true;
        public float valueForPlayer1, valueForPlayer2, macroStrategy;
        public float totalValue = 1;
        public TinyStrategy tinyStrategy = TinyStrategy.dontCare;

        public bool isAvailable = false;
        public static readonly int MINIMAX_WIN_RATE = 10;
        public static readonly int MINIMAX_LOSE_RATE = -10;
        public static readonly float GAME_OVER_VALUE = 10;
        private static readonly byte[] blankBoard = new byte[9];

        /// <summary>Creates an independent copy of a board.</summary>
        public object Clone()
        {
            TinyBoard b = TinyBoard.New(this.board, this.boardNum);
            b.boardNum = this.boardNum;
            b.canWin1 = this.canWin1;
            b.canWin2 = this.canWin2;
            b.isAvailable = this.isAvailable;
            b.macroStrategy = this.macroStrategy;
            b.tinyStrategy = this.tinyStrategy;
            b.valueForPlayer1 = this.valueForPlayer1;
            b.valueForPlayer2 = this.valueForPlayer2;
            b.totalValue = this.totalValue;
            b.winner = this.winner;
            return b;
        }

        /// <summary>Calculates total value based on strategy and personal values.</summary>
        public static float GetNotNormalizedTotalValue(TinyBoard b, int player, Settings s)
        {
            if (b.winner != -1)
                return GAME_OVER_VALUE;
            float val = 0;
            switch (player)
            {
                case 1:
                    val = b.valueForPlayer1 * s.PLAYER_VALUE_K.val + b.valueForPlayer2 * (2 - s.PLAYER_VALUE_K.val);
                    break;
                case 2:
                    val = b.valueForPlayer1 * (2 - s.PLAYER_VALUE_K.val) + b.valueForPlayer2 * s.PLAYER_VALUE_K.val;
                    break;
                default:
                    val = b.valueForPlayer1 * 1.0f + b.valueForPlayer2 * 1.0f;
                    break;
            }
            
            if (b.tinyStrategy == TinyStrategy.blockOpponent || b.tinyStrategy == TinyStrategy.winBoard)
                val += GAME_OVER_VALUE * s.SPECIAL_STRATEGY_BONUS_K.val;
            val += b.macroStrategy;
            return val;
        }

        /// <summary>Creates new simple TTT board.</summary>
        public static TinyBoard New(byte[] _board, int _boardNum = 0)
        {
            TinyBoard game = new TinyBoard();
            game.boardNum = _boardNum;
            game.board = _board.Clone() as byte[];
            return game;
        }

        /// <summary>Returns list of all empty cells on a board.</summary>
        public List<int> GetEmptyCells()
        {
            List<int> emptyCells = new List<int>();
            for (int i = 0; i < 9; i++)
            {
                if (this.board[i] == 0)
                    emptyCells.Add(i);
            }
            return emptyCells;
        }

        /// <summary>Get score for each possible move of a player.</summary>
        public float[] GetScoreTable(byte player, MacroBoard macro, Settings s)
        {
            var scores = new float[9];

            // Return if the game is already over
            if (winner != -1) return scores;

            // Get list of all available moves
            List<int> possibleMoves = this.GetEmptyCells();

            // If we don't care about this board, values of the cells are affected only by
            // total values of other boards
            if (this.tinyStrategy == TinyStrategy.dontCare)
            {
                List<float> weights = new List<float>();
                List<float> vals = new List<float>();
                List<int> emptyCells = this.GetEmptyCells();
                for (int i = 0; i < 9; i++)
                {
                    if (emptyCells.Contains(i))
                    {
                        vals.Add(1);
                        weights.Add(macro.boards[i].totalValue);
                    }
                    else
                    {
                        vals.Add(0);
                        weights.Add(0);
                    }
                }
                Utils.NormalizeValues(ref weights, 1);

                ApplyWeightsToList(ref vals, ref weights);
                return vals.ToArray();
            }

            // The empty board is hard to calculate so just give corresponding values
            if (possibleMoves.Count == 9)
            {
                List<float> weights = new List<float>();
                for (int i = 0; i < 9; i++)
                    weights.Add(macro.boards[i].totalValue);

                ShrinkWeights(ref weights, macro, s);

                Utils.NormalizeValues(ref weights, 1);

                List<float> vals =  new List<float> { 1.84f, 1.33f, 1.84f,
                                                    1.33f, 2.36f, 1.33f,
                                                    1.84f, 1.33f, 1.84f };
                ApplyWeightsToList(ref vals, ref weights);
                return vals.ToArray();

            }
            else
            {
                List<float> weights = new List<float>();
                for (int i = 0; i < 9; i++)
                    weights.Add(possibleMoves.Contains(i) ? macro.boards[i].totalValue : 0);
                
                ShrinkWeights(ref weights, macro, s);

                Utils.NormalizeValues(ref weights, 1);

                // Else for each possible move create a board and predict score
                foreach (int i in possibleMoves)
                {
                    var myBoard = this.Clone() as TinyBoard;
                    myBoard.board[i] = player;
                    MinimaxContext myMove = new MinimaxContext(Utils.SwitchPlayer(player), 0, macro, false);
                    scores[i] = myBoard.Minimax(myMove, player, (possibleMoves.Count <= 7), s);

                    // Estimate how valuable this cell is for opponent (but only if it won't cost extra resources)
                    if (possibleMoves.Count <= 6 || this.tinyStrategy == TinyStrategy.blockOpponent || 
                        this.tinyStrategy == TinyStrategy.winBoard)
                    {
                        var opBoard = this.Clone() as TinyBoard;
                        opBoard.board[i] = Utils.SwitchPlayer(player);
                        opBoard.tinyStrategy = TinyStrategy.winBoard;
                        MinimaxContext opMove = new MinimaxContext(player, 0, macro, false);
                        float opScore = opBoard.Minimax(opMove, Utils.SwitchPlayer(player), (possibleMoves.Count <= 7), s);

                        float k = 0.9f;
                        if (this.tinyStrategy == TinyStrategy.blockOpponent)
                            k = 0.7f;
                        scores[i] = k * scores[i] + (1 - k) * opScore;
                    }

                    // Scores are imbalanced here. Weights are too small comparing to them
                    scores[i] = (float) Math.Pow(scores[i], s.SCORE_TABLE_POW_K.val);

                    scores[i] = ApplyWeight(scores[i], weights[i]);
                }
                    
                return scores;
            }
        }

        /// <summary>Returns the probable player score for a next move.
        /// When the skipping is allowed the data is more accurate, but it takes more time to calculate.</summary>
        public float Minimax(MinimaxContext thisMove, int maxPlayer, bool allowSkipping, Settings s)
        {
            // Get winner in a predictive manner
            int winner = this.GetWinner();
            if (winner == -1 && !this.CanWin(1) && !this.CanWin(2))
                winner = 0;

            float branchScore = 0;

            // Check the current board state
            switch (winner)
            {
                // Continue branchig game
                case -1:
                    List<float> scores = new List<float>();
                    List<float> weights = new List<float>();

                    for (int i = 0; i < 9; i++)
                    {
                        if (this.board[i] == 0)
                        {
                            // For each possible move create a board and next move context
                            MinimaxContext newMove = new MinimaxContext(Utils.SwitchPlayer(thisMove.player),
                                                                    (byte)(thisMove.depth + 1),
                                                                    thisMove.macro,
                                                                    false);
                            var newBoard = this.Clone() as TinyBoard;
                            newBoard.board[i] = thisMove.player;
                            float newScore = newBoard.Minimax(newMove, maxPlayer, allowSkipping, s);
                            scores.Add(newScore);

                            // After the last move this board has a low weight
                            if (newBoard.GetWinner() != -1 && this.boardNum == i)
                            {
                                weights.Add(MacroBoard.GAME_OVER_WEIGHT);
                            }
                            else
                                weights.Add(thisMove.macro.boards[i].totalValue);
                        }
                        else
                        {
                            // This move can't be done
                            weights.Add(-1);
                            scores.Add(-1);
                        }
                    }

                    ShrinkWeights(ref weights, thisMove.macro, s);

                    if (allowSkipping && thisMove.justSkipped == false)
                    {
                        // What would happend if we skipped a turn
                        MinimaxContext newMove = new MinimaxContext(Utils.SwitchPlayer(thisMove.player),
                                                                    (byte)(thisMove.depth + 1),
                                                                    thisMove.macro,
                                                                    true);
                        var newBoard = this.Clone() as TinyBoard;
                        newBoard.tinyStrategy = this.tinyStrategy == TinyStrategy.blockOpponent ? TinyStrategy.winBoard :
                                                this.tinyStrategy == TinyStrategy.winBoard ? TinyStrategy.blockOpponent :
                                                this.tinyStrategy;
                        float newScore = newBoard.Minimax(newMove, maxPlayer, allowSkipping, s);
                        scores.Add(newScore);

                        // If we want to block our opponent we, should prevent any of his moves
                        weights.Add(this.tinyStrategy == TinyStrategy.blockOpponent ? 2f : 1.2f);
                    }
                    
                    // Remove impossible moves from scores list
                    for (int i = 0; i < 9; i++)
                        while (i < weights.Count && weights[i] < 0)
                        {
                            weights.RemoveAt(i);
                            scores.RemoveAt(i);
                        }

                    // Sort weights and scores arrays according to score value
                    for (int i = 1; i < scores.Count; i++)
                        for (int j = 0; j < i; j++)
                            if ((scores[i] * 1000 + weights[i] < scores[j] * 1000 + weights[j] && thisMove.player == maxPlayer) ||
                                (scores[i] * 1000 + weights[i] > scores[j] * 1000 + weights[j] && thisMove.player != maxPlayer))
                            {
                                float temp = scores[i];
                                scores[i] = scores[j];
                                scores[j] = temp;
                                temp = weights[i];
                                weights[i] = weights[j];
                                weights[j] = temp;
                            }

                    // At this point weight represents how the current player estimates the macro board consiquences
                    // of each move, but does not show, wich tiny board cell is more attractive to player.
                    // To make low score values more probable for a min player and vise versa we apply following modifications:
                    // (W represents how much each player wants to win on a tiny board)
                    float W = s.MINIMAX_EVALUATION_K.val;
                    for (int i = 0; i < weights.Count; i++)
                    {
                        weights[i] *= (float)Math.Pow(W, i * 10 / weights.Count);
                    }

                    // Normalize and apply cell weights
                    Utils.NormalizeValues(ref weights, 1);
                    scores = scores.Select<float, float>(x => Math.Max(x, 0)).ToList<float>();
                    ApplyWeightsToList(ref scores, ref weights);

                    branchScore = scores.Sum();
                    break;

                // Game is tie
                case 0:

                    // If we can't win this board or the plan is to block enemy moves,
                    // fast ties are more valuable than slow ones
                    if (tinyStrategy == TinyStrategy.blockOpponent 
                        || (!this.canWin1 && thisMove.player == 1) || (!this.canWin2 && thisMove.player == 2))
                    {
                        branchScore = MINIMAX_WIN_RATE / 2 - thisMove.depth * 1f;
                        branchScore = Math.Max(0.001f, branchScore);
                    } else
                    {
                        switch (this.tinyStrategy)
                        {
                            case TinyStrategy.basic:
                            case TinyStrategy.dontCare:
                                branchScore = thisMove.depth * 0.2f;
                                break;
                            case TinyStrategy.winBoard:
                                branchScore = thisMove.depth * 0.1f;
                                break;
                        }
                    }

                    // Make tie game more valuable if it is the plan
                    if (tinyStrategy == TinyStrategy.blockOpponent)
                        branchScore *= 1.5f;
                    break;

                // Somebody won
                case 1:
                case 2:
                    branchScore = (thisMove.player == maxPlayer) ? Math.Min(-1, MINIMAX_LOSE_RATE + 2 * (thisMove.depth + 1)): 
                                                                   Math.Max(1, MINIMAX_WIN_RATE - 2 * (thisMove.depth + 1));

                    // If we can win in one move and this board is a "must win" board, make this move no matter what
                    if (thisMove.depth == 0)
                    {
                        if (this.tinyStrategy == TinyStrategy.blockOpponent)
                            branchScore *= 8;
                        if (this.tinyStrategy == TinyStrategy.winBoard)
                            branchScore *= 15;
                    }

                    break;
            }

            return branchScore;
        }

        /// <summary>Get winner. -1 for not finished game, 0 for tie.</summary>
        public int GetWinner()
        {
            int winner = 0;

            // Check for empty cells
            for (int i = 0; i < 9; i++)
                if (board[i] == 0)
                {
                    winner = -1;
                    break;
                }

            // Check rows
            for (int i = 0; i < 3; i++)
                if ((board[i * 3] != 0) && (board[i * 3] == board[i * 3 + 1]) &&
                                            (board[i * 3 + 1] == board[i * 3 + 2]))
                {
                    winner = board[i * 3];
                    break;
                }

            // Check columns
            for (int j = 0; j < 3; j++)
                if ((board[j] != 0) && (board[j] == board[j + 3]) &&
                                           (board[j + 3] == board[j + 6]))
                {
                    winner = board[j];
                    break;
                }

            // Check diagonals
            if ((board[4] != 0) && ((board[0] == board[4] && board[4] == board[8]) ||
                                    (board[4] == board[2] && board[4] == board[6])))
                winner = board[4];

            return winner;
        }

        /// <summary>Check if given player can win.</summary>
        private bool CanWin(byte player)
        {

            if (this.GetWinner() != -1)
                return false;

            int enemy = Utils.SwitchPlayer(player);

            // Check rows
            for (int i = 0; i < 3; i++)
                if ((board[i * 3] != enemy) && ( board[i * 3 + 1] != enemy) &&
                                            (board[i * 3 + 2] != enemy))
                {
                    return true;
                }

            // Check columns
            for (int j = 0; j < 3; j++)
                if ((board[j] != enemy) && (board[j + 3] != enemy) &&
                                           (board[j + 6] != enemy))
                {
                    return true;
                }

            // Check diagonals
            if ((board[4] != enemy) && ((board[0] != enemy && board[8] != enemy) ||
                                    (board[2] != enemy && board[6] != enemy)))
                return true;

            return false;
        }

        /// <summary>Update values.</summary>
        public void UpdateValues(MacroBoard macro, Settings s)
        {
            canWin1 = this.CanWin(1);
            canWin2 = this.CanWin(2);
            this.winner = this.GetWinner();
            MacroBoard bufferMacro = macro.Clone() as MacroBoard;
            switch (this.winner) {
                case -1:
                    this.valueForPlayer1 = GetPlayerValue(bufferMacro, 1, s);
                    this.valueForPlayer2 = GetPlayerValue(bufferMacro, 2, s);
                    break;
                case 0:
                    this.valueForPlayer1 = this.valueForPlayer2 = 0;
                    break;
                default:
                    this.valueForPlayer1 = this.winner == 1 ? GAME_OVER_VALUE : -GAME_OVER_VALUE;
                    this.valueForPlayer2 = this.winner == 2 ? GAME_OVER_VALUE : -GAME_OVER_VALUE;
                    break;
            } 
        }

        /// <summary>Returns value for a given player.</summary>
        private float GetPlayerValue (MacroBoard macro, byte player, Settings s)
        {

            // Get a table of all macroboard weights
            List<int> availableCells = this.GetEmptyCells();
            List<float> weights = availableCells.Select<int, float>(x => 1 / TinyBoard.GetNotNormalizedTotalValue(macro.boards[x], player, s)).ToList<float>();
            for (int i = 0; i < 9; i++)
                if (!availableCells.Contains(i))
                    weights.Insert(i, 0);
            Utils.NormalizeValues(ref weights, this.totalValue);

            float playerValue = 0;

            // Find cells that are one step away from winning.
            // Those cells should not be treated as available
            // other way each of them will be counted as several ways to win
            List<int> winCells = new List<int>();
            foreach (int cell in availableCells)
            {
                TinyBoard newBoard = this.Clone() as TinyBoard;
                newBoard.board[cell] = player;
                if (newBoard.GetWinner() == player)
                {
                    playerValue += ApplyWeight(MINIMAX_WIN_RATE, weights[cell]);
                    winCells.Add(cell);
                }
            }

            // Check rows
            for (int i = 0; i < 3; i++)
            {
                float lineValue = MINIMAX_WIN_RATE;
                for (int j = 0; j < 3; j++)
                {
                    int b = i * 3 + j;
                    if (this.board[b] == Utils.SwitchPlayer(player) || winCells.Contains(b))
                    {
                        lineValue = 0;
                        break;
                    }
                    else if (availableCells.Contains(b))
                    {
                        lineValue = ApplyWeight(lineValue, weights[b]);
                    }
                }
                playerValue += lineValue;
            }

            // Check columns
            for (int i = 0; i < 3; i++)
            {
                float lineValue = MINIMAX_WIN_RATE;
                for (int j = 0; j < 3; j++)
                {
                    int b = j * 3 + i;
                    if (this.board[b] == Utils.SwitchPlayer(player) || winCells.Contains(b))
                    {
                        lineValue = 0;
                        break;
                    }
                    else if (availableCells.Contains(b))
                    {
                        lineValue = ApplyWeight(lineValue, weights[b]);
                    }
                }
                playerValue += lineValue;
            }

            // Check diagonals
            int[,] d = new int[,]
            {
                { 0, 4, 8 },
                { 2, 4, 6 }
            };
            for (int i = 0; i < 2; i++)
            {
                float lineValue = MINIMAX_WIN_RATE;
                for (int b = 0; b < 3; b++)
                    if (this.board[d[i, b]] == Utils.SwitchPlayer(player) || winCells.Contains(d[i, b]))
                    {
                        lineValue = 0;
                        break;
                    } else if (availableCells.Contains(d[i, b]))
                    {
                        lineValue = ApplyWeight(lineValue, weights[b]);
                    } 
                playerValue += lineValue;
            }

            playerValue = (float)Math.Pow(playerValue, s.PLAYER_VALUE_POW_K.val);

            return playerValue;
        }

        /// <summary>Modify minimax weights according to macro board state.</summary>
        public void ShrinkWeights (ref List<float> weights, MacroBoard macro, Settings s)
        {

            // The more valuable this board is, the less we care about other macro board values.
            // This does not apply to values that correspond to "must win" and "must not lose" boards.
            // K represents how much we care
            float K = s.SHRINK_K_BASIC.val;
            if (this.tinyStrategy == TinyStrategy.blockOpponent || this.tinyStrategy == TinyStrategy.winBoard)
                K = s.SHRINK_K_BLOCK.val;
            float mid = weights.Sum() / weights.Where<float>(x => x > 0).Count<float>();

            for (int i = 0; i < weights.Count; i++)
            {
                if (weights[i] <= 0)
                    continue;

                if (!((macro.boards[i].tinyStrategy == TinyStrategy.winBoard
                    || macro.boards[i].tinyStrategy == TinyStrategy.blockOpponent)
                    && this.totalValue < 1))
                {
                    weights[i] = mid + (weights[i] - mid) * (float)Math.Pow(this.totalValue, K);
                    weights[i] = Math.Max(0.001f, weights[i]);
                }
            }
        }

        /// <summary>Applies weight to value and returns the result.</summary>
        private float ApplyWeight (float value, float weight)
        {
            weight = Math.Max(weight, 0.001f);
            return value * weight;
        }

        /// <summary>Applies weight to an array.</summary>
        private void ApplyWeightsToList (ref List<float> vals, ref List<float> weights)
        {
            if (vals == null || weights == null || vals.Count > weights.Count)
                throw new ArgumentException("vals and weights");

            for (int i = 0; i < vals.Count; i++)
                vals[i] = ApplyWeight(vals[i], weights[i]);
        }
    }
}
