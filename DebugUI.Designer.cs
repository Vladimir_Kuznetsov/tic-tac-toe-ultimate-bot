﻿#if IS_DEBUG
namespace AIGames.UltimateTicTacToe.StarterBot
{
    partial class DebugUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnReset = new System.Windows.Forms.Button();
            this.BtnGo = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lab8 = new System.Windows.Forms.Label();
            this.lab5 = new System.Windows.Forms.Label();
            this.lab2 = new System.Windows.Forms.Label();
            this.lab7 = new System.Windows.Forms.Label();
            this.lab4 = new System.Windows.Forms.Label();
            this.lab1 = new System.Windows.Forms.Label();
            this.lab6 = new System.Windows.Forms.Label();
            this.lab3 = new System.Windows.Forms.Label();
            this.lab0 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.labStatus = new System.Windows.Forms.Label();
            this.lab9 = new System.Windows.Forms.Label();
            this.lab12 = new System.Windows.Forms.Label();
            this.lab15 = new System.Windows.Forms.Label();
            this.lab10 = new System.Windows.Forms.Label();
            this.lab13 = new System.Windows.Forms.Label();
            this.lab16 = new System.Windows.Forms.Label();
            this.lab11 = new System.Windows.Forms.Label();
            this.lab14 = new System.Windows.Forms.Label();
            this.lab17 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.labTotal = new System.Windows.Forms.Label();
            this.labStrategy = new System.Windows.Forms.Label();
            this.GeneticBtn = new System.Windows.Forms.Button();
            this.ManualBtn = new System.Windows.Forms.Button();
            this.pnlStrategy = new System.Windows.Forms.Panel();
            this.lab1_8 = new System.Windows.Forms.Label();
            this.lab1_5 = new System.Windows.Forms.Label();
            this.lab1_2 = new System.Windows.Forms.Label();
            this.lab1_7 = new System.Windows.Forms.Label();
            this.lab1_4 = new System.Windows.Forms.Label();
            this.lab1_1 = new System.Windows.Forms.Label();
            this.lab1_6 = new System.Windows.Forms.Label();
            this.lab1_3 = new System.Windows.Forms.Label();
            this.lab1_0 = new System.Windows.Forms.Label();
            this.pnlTotal = new System.Windows.Forms.Panel();
            this.lab0_8 = new System.Windows.Forms.Label();
            this.lab0_5 = new System.Windows.Forms.Label();
            this.lab0_2 = new System.Windows.Forms.Label();
            this.lab0_7 = new System.Windows.Forms.Label();
            this.lab0_4 = new System.Windows.Forms.Label();
            this.lab0_1 = new System.Windows.Forms.Label();
            this.lab0_6 = new System.Windows.Forms.Label();
            this.lab0_3 = new System.Windows.Forms.Label();
            this.lab0_0 = new System.Windows.Forms.Label();
            this.pnlBoard8 = new System.Windows.Forms.Panel();
            this.btn8_8 = new System.Windows.Forms.Button();
            this.btn8_7 = new System.Windows.Forms.Button();
            this.btn8_5 = new System.Windows.Forms.Button();
            this.btn8_4 = new System.Windows.Forms.Button();
            this.btn8_6 = new System.Windows.Forms.Button();
            this.btn8_2 = new System.Windows.Forms.Button();
            this.btn8_3 = new System.Windows.Forms.Button();
            this.btn8_1 = new System.Windows.Forms.Button();
            this.btn8_0 = new System.Windows.Forms.Button();
            this.pnlBoard5 = new System.Windows.Forms.Panel();
            this.btn5_8 = new System.Windows.Forms.Button();
            this.btn5_7 = new System.Windows.Forms.Button();
            this.btn5_5 = new System.Windows.Forms.Button();
            this.btn5_4 = new System.Windows.Forms.Button();
            this.btn5_6 = new System.Windows.Forms.Button();
            this.btn5_2 = new System.Windows.Forms.Button();
            this.btn5_3 = new System.Windows.Forms.Button();
            this.btn5_1 = new System.Windows.Forms.Button();
            this.btn5_0 = new System.Windows.Forms.Button();
            this.pnlBoard2 = new System.Windows.Forms.Panel();
            this.btn2_8 = new System.Windows.Forms.Button();
            this.btn2_7 = new System.Windows.Forms.Button();
            this.btn2_5 = new System.Windows.Forms.Button();
            this.btn2_4 = new System.Windows.Forms.Button();
            this.btn2_6 = new System.Windows.Forms.Button();
            this.btn2_2 = new System.Windows.Forms.Button();
            this.btn2_3 = new System.Windows.Forms.Button();
            this.btn2_1 = new System.Windows.Forms.Button();
            this.btn2_0 = new System.Windows.Forms.Button();
            this.pnlBoard7 = new System.Windows.Forms.Panel();
            this.btn7_8 = new System.Windows.Forms.Button();
            this.btn7_7 = new System.Windows.Forms.Button();
            this.btn7_5 = new System.Windows.Forms.Button();
            this.btn7_4 = new System.Windows.Forms.Button();
            this.btn7_6 = new System.Windows.Forms.Button();
            this.btn7_2 = new System.Windows.Forms.Button();
            this.btn7_3 = new System.Windows.Forms.Button();
            this.btn7_1 = new System.Windows.Forms.Button();
            this.btn7_0 = new System.Windows.Forms.Button();
            this.pnlBoard4 = new System.Windows.Forms.Panel();
            this.btn4_8 = new System.Windows.Forms.Button();
            this.btn4_7 = new System.Windows.Forms.Button();
            this.btn4_5 = new System.Windows.Forms.Button();
            this.btn4_4 = new System.Windows.Forms.Button();
            this.btn4_6 = new System.Windows.Forms.Button();
            this.btn4_2 = new System.Windows.Forms.Button();
            this.btn4_3 = new System.Windows.Forms.Button();
            this.btn4_1 = new System.Windows.Forms.Button();
            this.btn4_0 = new System.Windows.Forms.Button();
            this.pnlBoard1 = new System.Windows.Forms.Panel();
            this.btn1_8 = new System.Windows.Forms.Button();
            this.btn1_7 = new System.Windows.Forms.Button();
            this.btn1_5 = new System.Windows.Forms.Button();
            this.btn1_4 = new System.Windows.Forms.Button();
            this.btn1_6 = new System.Windows.Forms.Button();
            this.btn1_2 = new System.Windows.Forms.Button();
            this.btn1_3 = new System.Windows.Forms.Button();
            this.btn1_1 = new System.Windows.Forms.Button();
            this.btn1_0 = new System.Windows.Forms.Button();
            this.pnlBoard6 = new System.Windows.Forms.Panel();
            this.btn6_8 = new System.Windows.Forms.Button();
            this.btn6_7 = new System.Windows.Forms.Button();
            this.btn6_5 = new System.Windows.Forms.Button();
            this.btn6_4 = new System.Windows.Forms.Button();
            this.btn6_6 = new System.Windows.Forms.Button();
            this.btn6_2 = new System.Windows.Forms.Button();
            this.btn6_3 = new System.Windows.Forms.Button();
            this.btn6_1 = new System.Windows.Forms.Button();
            this.btn6_0 = new System.Windows.Forms.Button();
            this.pnlBoard3 = new System.Windows.Forms.Panel();
            this.btn3_8 = new System.Windows.Forms.Button();
            this.btn3_7 = new System.Windows.Forms.Button();
            this.btn3_5 = new System.Windows.Forms.Button();
            this.btn3_4 = new System.Windows.Forms.Button();
            this.btn3_6 = new System.Windows.Forms.Button();
            this.btn3_2 = new System.Windows.Forms.Button();
            this.btn3_3 = new System.Windows.Forms.Button();
            this.btn3_1 = new System.Windows.Forms.Button();
            this.btn3_0 = new System.Windows.Forms.Button();
            this.pnlBoard0 = new System.Windows.Forms.Panel();
            this.btn0_8 = new System.Windows.Forms.Button();
            this.btn0_7 = new System.Windows.Forms.Button();
            this.btn0_5 = new System.Windows.Forms.Button();
            this.btn0_4 = new System.Windows.Forms.Button();
            this.btn0_6 = new System.Windows.Forms.Button();
            this.btn0_2 = new System.Windows.Forms.Button();
            this.btn0_3 = new System.Windows.Forms.Button();
            this.btn0_1 = new System.Windows.Forms.Button();
            this.btn0_0 = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.nudNumber = new System.Windows.Forms.NumericUpDown();
            this.panel16 = new System.Windows.Forms.Panel();
            this.tbWeight8 = new System.Windows.Forms.TextBox();
            this.tbWeight7 = new System.Windows.Forms.TextBox();
            this.tbWeight5 = new System.Windows.Forms.TextBox();
            this.tbWeight4 = new System.Windows.Forms.TextBox();
            this.tbWeight6 = new System.Windows.Forms.TextBox();
            this.tbWeight2 = new System.Windows.Forms.TextBox();
            this.tbWeight3 = new System.Windows.Forms.TextBox();
            this.tbWeight1 = new System.Windows.Forms.TextBox();
            this.tbWeight0 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.pnlStrategy.SuspendLayout();
            this.pnlTotal.SuspendLayout();
            this.pnlBoard8.SuspendLayout();
            this.pnlBoard5.SuspendLayout();
            this.pnlBoard2.SuspendLayout();
            this.pnlBoard7.SuspendLayout();
            this.pnlBoard4.SuspendLayout();
            this.pnlBoard1.SuspendLayout();
            this.pnlBoard6.SuspendLayout();
            this.pnlBoard3.SuspendLayout();
            this.pnlBoard0.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumber)).BeginInit();
            this.panel16.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnReset
            // 
            this.BtnReset.Location = new System.Drawing.Point(445, 20);
            this.BtnReset.Name = "BtnReset";
            this.BtnReset.Size = new System.Drawing.Size(82, 23);
            this.BtnReset.TabIndex = 4;
            this.BtnReset.Text = "Reset";
            this.BtnReset.UseVisualStyleBackColor = true;
            this.BtnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // BtnGo
            // 
            this.BtnGo.Location = new System.Drawing.Point(358, 20);
            this.BtnGo.Name = "BtnGo";
            this.BtnGo.Size = new System.Drawing.Size(82, 23);
            this.BtnGo.TabIndex = 5;
            this.BtnGo.Text = "Go";
            this.BtnGo.UseVisualStyleBackColor = true;
            this.BtnGo.Click += new System.EventHandler(this.BtnGo_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lab8);
            this.panel2.Controls.Add(this.lab5);
            this.panel2.Controls.Add(this.lab2);
            this.panel2.Controls.Add(this.lab7);
            this.panel2.Controls.Add(this.lab4);
            this.panel2.Controls.Add(this.lab1);
            this.panel2.Controls.Add(this.lab6);
            this.panel2.Controls.Add(this.lab3);
            this.panel2.Controls.Add(this.lab0);
            this.panel2.Location = new System.Drawing.Point(7, 181);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(170, 169);
            this.panel2.TabIndex = 2;
            // 
            // lab8
            // 
            this.lab8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab8.Location = new System.Drawing.Point(112, 112);
            this.lab8.Name = "lab8";
            this.lab8.Size = new System.Drawing.Size(43, 43);
            this.lab8.TabIndex = 2;
            this.lab8.Text = "0";
            this.lab8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab5
            // 
            this.lab5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab5.Location = new System.Drawing.Point(112, 63);
            this.lab5.Name = "lab5";
            this.lab5.Size = new System.Drawing.Size(43, 43);
            this.lab5.TabIndex = 2;
            this.lab5.Text = "0";
            this.lab5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab2
            // 
            this.lab2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab2.Location = new System.Drawing.Point(112, 14);
            this.lab2.Name = "lab2";
            this.lab2.Size = new System.Drawing.Size(43, 43);
            this.lab2.TabIndex = 2;
            this.lab2.Text = "0";
            this.lab2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab7
            // 
            this.lab7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab7.Location = new System.Drawing.Point(63, 112);
            this.lab7.Name = "lab7";
            this.lab7.Size = new System.Drawing.Size(43, 43);
            this.lab7.TabIndex = 2;
            this.lab7.Text = "0";
            this.lab7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab4
            // 
            this.lab4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab4.Location = new System.Drawing.Point(63, 63);
            this.lab4.Name = "lab4";
            this.lab4.Size = new System.Drawing.Size(43, 43);
            this.lab4.TabIndex = 2;
            this.lab4.Text = "0";
            this.lab4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab1
            // 
            this.lab1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab1.Location = new System.Drawing.Point(63, 14);
            this.lab1.Name = "lab1";
            this.lab1.Size = new System.Drawing.Size(43, 43);
            this.lab1.TabIndex = 2;
            this.lab1.Text = "0";
            this.lab1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab6
            // 
            this.lab6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab6.Location = new System.Drawing.Point(14, 112);
            this.lab6.Name = "lab6";
            this.lab6.Size = new System.Drawing.Size(43, 43);
            this.lab6.TabIndex = 2;
            this.lab6.Text = "0";
            this.lab6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab3
            // 
            this.lab3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab3.Location = new System.Drawing.Point(14, 63);
            this.lab3.Name = "lab3";
            this.lab3.Size = new System.Drawing.Size(43, 43);
            this.lab3.TabIndex = 2;
            this.lab3.Text = "0";
            this.lab3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab0
            // 
            this.lab0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab0.Location = new System.Drawing.Point(14, 14);
            this.lab0.Name = "lab0";
            this.lab0.Size = new System.Drawing.Size(43, 43);
            this.lab0.TabIndex = 2;
            this.lab0.Text = "0";
            this.lab0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn8);
            this.panel1.Controls.Add(this.btn7);
            this.panel1.Controls.Add(this.btn5);
            this.panel1.Controls.Add(this.btn4);
            this.panel1.Controls.Add(this.btn6);
            this.panel1.Controls.Add(this.btn2);
            this.panel1.Controls.Add(this.btn3);
            this.panel1.Controls.Add(this.btn1);
            this.panel1.Controls.Add(this.btn0);
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(170, 169);
            this.panel1.TabIndex = 3;
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn8.Location = new System.Drawing.Point(112, 112);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(43, 43);
            this.btn8.TabIndex = 0;
            this.btn8.Text = " ";
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn7.Location = new System.Drawing.Point(63, 112);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(43, 43);
            this.btn7.TabIndex = 0;
            this.btn7.Text = " ";
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn5.Location = new System.Drawing.Point(112, 63);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(43, 43);
            this.btn5.TabIndex = 0;
            this.btn5.Text = " ";
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn4.Location = new System.Drawing.Point(63, 63);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(43, 43);
            this.btn4.TabIndex = 0;
            this.btn4.Text = " ";
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn6.Location = new System.Drawing.Point(14, 112);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(43, 43);
            this.btn6.TabIndex = 0;
            this.btn6.Text = " ";
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn2.Location = new System.Drawing.Point(112, 14);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(43, 43);
            this.btn2.TabIndex = 0;
            this.btn2.Text = " ";
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn3.Location = new System.Drawing.Point(14, 63);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(43, 43);
            this.btn3.TabIndex = 0;
            this.btn3.Text = " ";
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn1.Location = new System.Drawing.Point(63, 14);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(43, 43);
            this.btn1.TabIndex = 0;
            this.btn1.Text = " ";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn0
            // 
            this.btn0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn0.Location = new System.Drawing.Point(14, 14);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(43, 43);
            this.btn0.TabIndex = 0;
            this.btn0.Text = " ";
            this.btn0.UseVisualStyleBackColor = false;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // labStatus
            // 
            this.labStatus.Location = new System.Drawing.Point(358, 77);
            this.labStatus.Name = "labStatus";
            this.labStatus.Size = new System.Drawing.Size(169, 114);
            this.labStatus.TabIndex = 6;
            // 
            // lab9
            // 
            this.lab9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab9.Location = new System.Drawing.Point(14, 14);
            this.lab9.Name = "lab9";
            this.lab9.Size = new System.Drawing.Size(43, 43);
            this.lab9.TabIndex = 2;
            this.lab9.Text = "0";
            this.lab9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab12
            // 
            this.lab12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab12.Location = new System.Drawing.Point(14, 63);
            this.lab12.Name = "lab12";
            this.lab12.Size = new System.Drawing.Size(43, 43);
            this.lab12.TabIndex = 2;
            this.lab12.Text = "0";
            this.lab12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab15
            // 
            this.lab15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab15.Location = new System.Drawing.Point(14, 112);
            this.lab15.Name = "lab15";
            this.lab15.Size = new System.Drawing.Size(43, 43);
            this.lab15.TabIndex = 2;
            this.lab15.Text = "0";
            this.lab15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab10
            // 
            this.lab10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab10.Location = new System.Drawing.Point(63, 14);
            this.lab10.Name = "lab10";
            this.lab10.Size = new System.Drawing.Size(43, 43);
            this.lab10.TabIndex = 2;
            this.lab10.Text = "0";
            this.lab10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab13
            // 
            this.lab13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab13.Location = new System.Drawing.Point(63, 63);
            this.lab13.Name = "lab13";
            this.lab13.Size = new System.Drawing.Size(43, 43);
            this.lab13.TabIndex = 2;
            this.lab13.Text = "0";
            this.lab13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab16
            // 
            this.lab16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab16.Location = new System.Drawing.Point(63, 112);
            this.lab16.Name = "lab16";
            this.lab16.Size = new System.Drawing.Size(43, 43);
            this.lab16.TabIndex = 2;
            this.lab16.Text = "0";
            this.lab16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab11
            // 
            this.lab11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab11.Location = new System.Drawing.Point(112, 14);
            this.lab11.Name = "lab11";
            this.lab11.Size = new System.Drawing.Size(43, 43);
            this.lab11.TabIndex = 2;
            this.lab11.Text = "0";
            this.lab11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab14
            // 
            this.lab14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab14.Location = new System.Drawing.Point(112, 63);
            this.lab14.Name = "lab14";
            this.lab14.Size = new System.Drawing.Size(43, 43);
            this.lab14.TabIndex = 2;
            this.lab14.Text = "0";
            this.lab14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab17
            // 
            this.lab17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab17.Location = new System.Drawing.Point(112, 112);
            this.lab17.Name = "lab17";
            this.lab17.Size = new System.Drawing.Size(43, 43);
            this.lab17.TabIndex = 2;
            this.lab17.Text = "0";
            this.lab17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lab17);
            this.panel3.Controls.Add(this.lab14);
            this.panel3.Controls.Add(this.lab11);
            this.panel3.Controls.Add(this.lab16);
            this.panel3.Controls.Add(this.lab13);
            this.panel3.Controls.Add(this.lab10);
            this.panel3.Controls.Add(this.lab15);
            this.panel3.Controls.Add(this.lab12);
            this.panel3.Controls.Add(this.lab9);
            this.panel3.Location = new System.Drawing.Point(183, 181);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(170, 169);
            this.panel3.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(543, 393);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtLog);
            this.tabPage2.Controls.Add(this.labTotal);
            this.tabPage2.Controls.Add(this.labStrategy);
            this.tabPage2.Controls.Add(this.GeneticBtn);
            this.tabPage2.Controls.Add(this.ManualBtn);
            this.tabPage2.Controls.Add(this.pnlStrategy);
            this.tabPage2.Controls.Add(this.pnlTotal);
            this.tabPage2.Controls.Add(this.pnlBoard8);
            this.tabPage2.Controls.Add(this.pnlBoard5);
            this.tabPage2.Controls.Add(this.pnlBoard2);
            this.tabPage2.Controls.Add(this.pnlBoard7);
            this.tabPage2.Controls.Add(this.pnlBoard4);
            this.tabPage2.Controls.Add(this.pnlBoard1);
            this.tabPage2.Controls.Add(this.pnlBoard6);
            this.tabPage2.Controls.Add(this.pnlBoard3);
            this.tabPage2.Controls.Add(this.pnlBoard0);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(535, 367);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ultimate";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtLog
            // 
            this.txtLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtLog.Location = new System.Drawing.Point(381, 81);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.Size = new System.Drawing.Size(146, 298);
            this.txtLog.TabIndex = 8;
            this.txtLog.Visible = false;
            // 
            // labTotal
            // 
            this.labTotal.Location = new System.Drawing.Point(385, 92);
            this.labTotal.Name = "labTotal";
            this.labTotal.Size = new System.Drawing.Size(126, 24);
            this.labTotal.TabIndex = 7;
            this.labTotal.Text = "Total value";
            this.labTotal.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labStrategy
            // 
            this.labStrategy.Location = new System.Drawing.Point(385, 234);
            this.labStrategy.Name = "labStrategy";
            this.labStrategy.Size = new System.Drawing.Size(126, 24);
            this.labStrategy.TabIndex = 7;
            this.labStrategy.Text = "Strategy";
            this.labStrategy.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // GeneticBtn
            // 
            this.GeneticBtn.Location = new System.Drawing.Point(381, 45);
            this.GeneticBtn.Name = "GeneticBtn";
            this.GeneticBtn.Size = new System.Drawing.Size(146, 27);
            this.GeneticBtn.TabIndex = 6;
            this.GeneticBtn.Text = "Start Genetic";
            this.GeneticBtn.UseVisualStyleBackColor = true;
            this.GeneticBtn.Click += new System.EventHandler(this.GeneticBtn_Click);
            // 
            // ManualBtn
            // 
            this.ManualBtn.Location = new System.Drawing.Point(381, 12);
            this.ManualBtn.Name = "ManualBtn";
            this.ManualBtn.Size = new System.Drawing.Size(146, 27);
            this.ManualBtn.TabIndex = 6;
            this.ManualBtn.Text = "Manual";
            this.ManualBtn.UseVisualStyleBackColor = true;
            this.ManualBtn.Click += new System.EventHandler(this.ManualBtn_Click);
            // 
            // pnlStrategy
            // 
            this.pnlStrategy.Controls.Add(this.lab1_8);
            this.pnlStrategy.Controls.Add(this.lab1_5);
            this.pnlStrategy.Controls.Add(this.lab1_2);
            this.pnlStrategy.Controls.Add(this.lab1_7);
            this.pnlStrategy.Controls.Add(this.lab1_4);
            this.pnlStrategy.Controls.Add(this.lab1_1);
            this.pnlStrategy.Controls.Add(this.lab1_6);
            this.pnlStrategy.Controls.Add(this.lab1_3);
            this.pnlStrategy.Controls.Add(this.lab1_0);
            this.pnlStrategy.Location = new System.Drawing.Point(388, 257);
            this.pnlStrategy.Name = "pnlStrategy";
            this.pnlStrategy.Size = new System.Drawing.Size(123, 122);
            this.pnlStrategy.TabIndex = 5;
            // 
            // lab1_8
            // 
            this.lab1_8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab1_8.Location = new System.Drawing.Point(85, 84);
            this.lab1_8.Name = "lab1_8";
            this.lab1_8.Size = new System.Drawing.Size(32, 32);
            this.lab1_8.TabIndex = 2;
            this.lab1_8.Text = "0";
            this.lab1_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab1_5
            // 
            this.lab1_5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab1_5.Location = new System.Drawing.Point(85, 46);
            this.lab1_5.Name = "lab1_5";
            this.lab1_5.Size = new System.Drawing.Size(32, 32);
            this.lab1_5.TabIndex = 2;
            this.lab1_5.Text = "0";
            this.lab1_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab1_2
            // 
            this.lab1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab1_2.Location = new System.Drawing.Point(85, 8);
            this.lab1_2.Name = "lab1_2";
            this.lab1_2.Size = new System.Drawing.Size(32, 32);
            this.lab1_2.TabIndex = 2;
            this.lab1_2.Text = "0";
            this.lab1_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab1_7
            // 
            this.lab1_7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab1_7.Location = new System.Drawing.Point(47, 84);
            this.lab1_7.Name = "lab1_7";
            this.lab1_7.Size = new System.Drawing.Size(32, 32);
            this.lab1_7.TabIndex = 2;
            this.lab1_7.Text = "0";
            this.lab1_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab1_4
            // 
            this.lab1_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab1_4.Location = new System.Drawing.Point(47, 46);
            this.lab1_4.Name = "lab1_4";
            this.lab1_4.Size = new System.Drawing.Size(32, 32);
            this.lab1_4.TabIndex = 2;
            this.lab1_4.Text = "0";
            this.lab1_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab1_1
            // 
            this.lab1_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab1_1.Location = new System.Drawing.Point(47, 8);
            this.lab1_1.Name = "lab1_1";
            this.lab1_1.Size = new System.Drawing.Size(32, 32);
            this.lab1_1.TabIndex = 2;
            this.lab1_1.Text = "0";
            this.lab1_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab1_6
            // 
            this.lab1_6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab1_6.Location = new System.Drawing.Point(9, 84);
            this.lab1_6.Name = "lab1_6";
            this.lab1_6.Size = new System.Drawing.Size(32, 32);
            this.lab1_6.TabIndex = 2;
            this.lab1_6.Text = "0";
            this.lab1_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab1_3
            // 
            this.lab1_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab1_3.Location = new System.Drawing.Point(9, 46);
            this.lab1_3.Name = "lab1_3";
            this.lab1_3.Size = new System.Drawing.Size(32, 32);
            this.lab1_3.TabIndex = 2;
            this.lab1_3.Text = "0";
            this.lab1_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab1_0
            // 
            this.lab1_0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab1_0.Location = new System.Drawing.Point(9, 8);
            this.lab1_0.Name = "lab1_0";
            this.lab1_0.Size = new System.Drawing.Size(32, 32);
            this.lab1_0.TabIndex = 2;
            this.lab1_0.Text = "0";
            this.lab1_0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlTotal
            // 
            this.pnlTotal.Controls.Add(this.lab0_8);
            this.pnlTotal.Controls.Add(this.lab0_5);
            this.pnlTotal.Controls.Add(this.lab0_2);
            this.pnlTotal.Controls.Add(this.lab0_7);
            this.pnlTotal.Controls.Add(this.lab0_4);
            this.pnlTotal.Controls.Add(this.lab0_1);
            this.pnlTotal.Controls.Add(this.lab0_6);
            this.pnlTotal.Controls.Add(this.lab0_3);
            this.pnlTotal.Controls.Add(this.lab0_0);
            this.pnlTotal.Location = new System.Drawing.Point(388, 114);
            this.pnlTotal.Name = "pnlTotal";
            this.pnlTotal.Size = new System.Drawing.Size(123, 122);
            this.pnlTotal.TabIndex = 5;
            // 
            // lab0_8
            // 
            this.lab0_8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab0_8.Location = new System.Drawing.Point(85, 84);
            this.lab0_8.Name = "lab0_8";
            this.lab0_8.Size = new System.Drawing.Size(32, 32);
            this.lab0_8.TabIndex = 2;
            this.lab0_8.Text = "0";
            this.lab0_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab0_5
            // 
            this.lab0_5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab0_5.Location = new System.Drawing.Point(85, 46);
            this.lab0_5.Name = "lab0_5";
            this.lab0_5.Size = new System.Drawing.Size(32, 32);
            this.lab0_5.TabIndex = 2;
            this.lab0_5.Text = "0";
            this.lab0_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab0_2
            // 
            this.lab0_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab0_2.Location = new System.Drawing.Point(85, 8);
            this.lab0_2.Name = "lab0_2";
            this.lab0_2.Size = new System.Drawing.Size(32, 32);
            this.lab0_2.TabIndex = 2;
            this.lab0_2.Text = "0";
            this.lab0_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab0_7
            // 
            this.lab0_7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab0_7.Location = new System.Drawing.Point(47, 84);
            this.lab0_7.Name = "lab0_7";
            this.lab0_7.Size = new System.Drawing.Size(32, 32);
            this.lab0_7.TabIndex = 2;
            this.lab0_7.Text = "0";
            this.lab0_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab0_4
            // 
            this.lab0_4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab0_4.Location = new System.Drawing.Point(47, 46);
            this.lab0_4.Name = "lab0_4";
            this.lab0_4.Size = new System.Drawing.Size(32, 32);
            this.lab0_4.TabIndex = 2;
            this.lab0_4.Text = "0";
            this.lab0_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab0_1
            // 
            this.lab0_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab0_1.Location = new System.Drawing.Point(47, 8);
            this.lab0_1.Name = "lab0_1";
            this.lab0_1.Size = new System.Drawing.Size(32, 32);
            this.lab0_1.TabIndex = 2;
            this.lab0_1.Text = "0";
            this.lab0_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab0_6
            // 
            this.lab0_6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab0_6.Location = new System.Drawing.Point(9, 84);
            this.lab0_6.Name = "lab0_6";
            this.lab0_6.Size = new System.Drawing.Size(32, 32);
            this.lab0_6.TabIndex = 2;
            this.lab0_6.Text = "0";
            this.lab0_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab0_3
            // 
            this.lab0_3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab0_3.Location = new System.Drawing.Point(9, 46);
            this.lab0_3.Name = "lab0_3";
            this.lab0_3.Size = new System.Drawing.Size(32, 32);
            this.lab0_3.TabIndex = 2;
            this.lab0_3.Text = "0";
            this.lab0_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lab0_0
            // 
            this.lab0_0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lab0_0.Location = new System.Drawing.Point(9, 8);
            this.lab0_0.Name = "lab0_0";
            this.lab0_0.Size = new System.Drawing.Size(32, 32);
            this.lab0_0.TabIndex = 2;
            this.lab0_0.Text = "0";
            this.lab0_0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlBoard8
            // 
            this.pnlBoard8.Controls.Add(this.btn8_8);
            this.pnlBoard8.Controls.Add(this.btn8_7);
            this.pnlBoard8.Controls.Add(this.btn8_5);
            this.pnlBoard8.Controls.Add(this.btn8_4);
            this.pnlBoard8.Controls.Add(this.btn8_6);
            this.pnlBoard8.Controls.Add(this.btn8_2);
            this.pnlBoard8.Controls.Add(this.btn8_3);
            this.pnlBoard8.Controls.Add(this.btn8_1);
            this.pnlBoard8.Controls.Add(this.btn8_0);
            this.pnlBoard8.Location = new System.Drawing.Point(250, 262);
            this.pnlBoard8.Name = "pnlBoard8";
            this.pnlBoard8.Size = new System.Drawing.Size(115, 122);
            this.pnlBoard8.TabIndex = 4;
            // 
            // btn8_8
            // 
            this.btn8_8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn8_8.Location = new System.Drawing.Point(78, 82);
            this.btn8_8.Name = "btn8_8";
            this.btn8_8.Size = new System.Drawing.Size(30, 32);
            this.btn8_8.TabIndex = 0;
            this.btn8_8.Text = " ";
            this.btn8_8.UseVisualStyleBackColor = false;
            this.btn8_8.Click += new System.EventHandler(this.btn8_8_Click);
            // 
            // btn8_7
            // 
            this.btn8_7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn8_7.Location = new System.Drawing.Point(42, 82);
            this.btn8_7.Name = "btn8_7";
            this.btn8_7.Size = new System.Drawing.Size(30, 32);
            this.btn8_7.TabIndex = 0;
            this.btn8_7.Text = " ";
            this.btn8_7.UseVisualStyleBackColor = false;
            this.btn8_7.Click += new System.EventHandler(this.btn8_7_Click);
            // 
            // btn8_5
            // 
            this.btn8_5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn8_5.Location = new System.Drawing.Point(78, 44);
            this.btn8_5.Name = "btn8_5";
            this.btn8_5.Size = new System.Drawing.Size(30, 32);
            this.btn8_5.TabIndex = 0;
            this.btn8_5.Text = " ";
            this.btn8_5.UseVisualStyleBackColor = false;
            this.btn8_5.Click += new System.EventHandler(this.btn8_5_Click);
            // 
            // btn8_4
            // 
            this.btn8_4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn8_4.Location = new System.Drawing.Point(42, 44);
            this.btn8_4.Name = "btn8_4";
            this.btn8_4.Size = new System.Drawing.Size(30, 32);
            this.btn8_4.TabIndex = 0;
            this.btn8_4.Text = " ";
            this.btn8_4.UseVisualStyleBackColor = false;
            this.btn8_4.Click += new System.EventHandler(this.btn8_4_Click);
            // 
            // btn8_6
            // 
            this.btn8_6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn8_6.Location = new System.Drawing.Point(6, 82);
            this.btn8_6.Name = "btn8_6";
            this.btn8_6.Size = new System.Drawing.Size(30, 32);
            this.btn8_6.TabIndex = 0;
            this.btn8_6.Text = " ";
            this.btn8_6.UseVisualStyleBackColor = false;
            this.btn8_6.Click += new System.EventHandler(this.btn8_6_Click);
            // 
            // btn8_2
            // 
            this.btn8_2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn8_2.Location = new System.Drawing.Point(78, 6);
            this.btn8_2.Name = "btn8_2";
            this.btn8_2.Size = new System.Drawing.Size(30, 32);
            this.btn8_2.TabIndex = 0;
            this.btn8_2.Text = " ";
            this.btn8_2.UseVisualStyleBackColor = false;
            this.btn8_2.Click += new System.EventHandler(this.btn8_2_Click);
            // 
            // btn8_3
            // 
            this.btn8_3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn8_3.Location = new System.Drawing.Point(6, 44);
            this.btn8_3.Name = "btn8_3";
            this.btn8_3.Size = new System.Drawing.Size(30, 32);
            this.btn8_3.TabIndex = 0;
            this.btn8_3.Text = " ";
            this.btn8_3.UseVisualStyleBackColor = false;
            this.btn8_3.Click += new System.EventHandler(this.btn8_3_Click);
            // 
            // btn8_1
            // 
            this.btn8_1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn8_1.Location = new System.Drawing.Point(42, 6);
            this.btn8_1.Name = "btn8_1";
            this.btn8_1.Size = new System.Drawing.Size(30, 32);
            this.btn8_1.TabIndex = 0;
            this.btn8_1.Text = " ";
            this.btn8_1.UseVisualStyleBackColor = false;
            this.btn8_1.Click += new System.EventHandler(this.btn8_1_Click);
            // 
            // btn8_0
            // 
            this.btn8_0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn8_0.Location = new System.Drawing.Point(6, 6);
            this.btn8_0.Name = "btn8_0";
            this.btn8_0.Size = new System.Drawing.Size(30, 32);
            this.btn8_0.TabIndex = 0;
            this.btn8_0.Text = " ";
            this.btn8_0.UseVisualStyleBackColor = false;
            this.btn8_0.Click += new System.EventHandler(this.btn8_0_Click);
            // 
            // pnlBoard5
            // 
            this.pnlBoard5.Controls.Add(this.btn5_8);
            this.pnlBoard5.Controls.Add(this.btn5_7);
            this.pnlBoard5.Controls.Add(this.btn5_5);
            this.pnlBoard5.Controls.Add(this.btn5_4);
            this.pnlBoard5.Controls.Add(this.btn5_6);
            this.pnlBoard5.Controls.Add(this.btn5_2);
            this.pnlBoard5.Controls.Add(this.btn5_3);
            this.pnlBoard5.Controls.Add(this.btn5_1);
            this.pnlBoard5.Controls.Add(this.btn5_0);
            this.pnlBoard5.Location = new System.Drawing.Point(250, 134);
            this.pnlBoard5.Name = "pnlBoard5";
            this.pnlBoard5.Size = new System.Drawing.Size(115, 122);
            this.pnlBoard5.TabIndex = 4;
            // 
            // btn5_8
            // 
            this.btn5_8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn5_8.Location = new System.Drawing.Point(78, 82);
            this.btn5_8.Name = "btn5_8";
            this.btn5_8.Size = new System.Drawing.Size(30, 32);
            this.btn5_8.TabIndex = 0;
            this.btn5_8.Text = " ";
            this.btn5_8.UseVisualStyleBackColor = false;
            this.btn5_8.Click += new System.EventHandler(this.btn5_8_Click);
            // 
            // btn5_7
            // 
            this.btn5_7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn5_7.Location = new System.Drawing.Point(42, 82);
            this.btn5_7.Name = "btn5_7";
            this.btn5_7.Size = new System.Drawing.Size(30, 32);
            this.btn5_7.TabIndex = 0;
            this.btn5_7.Text = " ";
            this.btn5_7.UseVisualStyleBackColor = false;
            this.btn5_7.Click += new System.EventHandler(this.btn5_7_Click);
            // 
            // btn5_5
            // 
            this.btn5_5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn5_5.Location = new System.Drawing.Point(78, 44);
            this.btn5_5.Name = "btn5_5";
            this.btn5_5.Size = new System.Drawing.Size(30, 32);
            this.btn5_5.TabIndex = 0;
            this.btn5_5.Text = " ";
            this.btn5_5.UseVisualStyleBackColor = false;
            this.btn5_5.Click += new System.EventHandler(this.btn5_5_Click);
            // 
            // btn5_4
            // 
            this.btn5_4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn5_4.Location = new System.Drawing.Point(42, 44);
            this.btn5_4.Name = "btn5_4";
            this.btn5_4.Size = new System.Drawing.Size(30, 32);
            this.btn5_4.TabIndex = 0;
            this.btn5_4.Text = " ";
            this.btn5_4.UseVisualStyleBackColor = false;
            this.btn5_4.Click += new System.EventHandler(this.btn5_4_Click);
            // 
            // btn5_6
            // 
            this.btn5_6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn5_6.Location = new System.Drawing.Point(6, 82);
            this.btn5_6.Name = "btn5_6";
            this.btn5_6.Size = new System.Drawing.Size(30, 32);
            this.btn5_6.TabIndex = 0;
            this.btn5_6.Text = " ";
            this.btn5_6.UseVisualStyleBackColor = false;
            this.btn5_6.Click += new System.EventHandler(this.btn5_6_Click);
            // 
            // btn5_2
            // 
            this.btn5_2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn5_2.Location = new System.Drawing.Point(78, 6);
            this.btn5_2.Name = "btn5_2";
            this.btn5_2.Size = new System.Drawing.Size(30, 32);
            this.btn5_2.TabIndex = 0;
            this.btn5_2.Text = " ";
            this.btn5_2.UseVisualStyleBackColor = false;
            this.btn5_2.Click += new System.EventHandler(this.btn5_2_Click);
            // 
            // btn5_3
            // 
            this.btn5_3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn5_3.Location = new System.Drawing.Point(6, 44);
            this.btn5_3.Name = "btn5_3";
            this.btn5_3.Size = new System.Drawing.Size(30, 32);
            this.btn5_3.TabIndex = 0;
            this.btn5_3.Text = " ";
            this.btn5_3.UseVisualStyleBackColor = false;
            this.btn5_3.Click += new System.EventHandler(this.btn5_3_Click);
            // 
            // btn5_1
            // 
            this.btn5_1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn5_1.Location = new System.Drawing.Point(42, 6);
            this.btn5_1.Name = "btn5_1";
            this.btn5_1.Size = new System.Drawing.Size(30, 32);
            this.btn5_1.TabIndex = 0;
            this.btn5_1.Text = " ";
            this.btn5_1.UseVisualStyleBackColor = false;
            this.btn5_1.Click += new System.EventHandler(this.btn5_1_Click);
            // 
            // btn5_0
            // 
            this.btn5_0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn5_0.Location = new System.Drawing.Point(6, 6);
            this.btn5_0.Name = "btn5_0";
            this.btn5_0.Size = new System.Drawing.Size(30, 32);
            this.btn5_0.TabIndex = 0;
            this.btn5_0.Text = " ";
            this.btn5_0.UseVisualStyleBackColor = false;
            this.btn5_0.Click += new System.EventHandler(this.btn5_0_Click);
            // 
            // pnlBoard2
            // 
            this.pnlBoard2.Controls.Add(this.btn2_8);
            this.pnlBoard2.Controls.Add(this.btn2_7);
            this.pnlBoard2.Controls.Add(this.btn2_5);
            this.pnlBoard2.Controls.Add(this.btn2_4);
            this.pnlBoard2.Controls.Add(this.btn2_6);
            this.pnlBoard2.Controls.Add(this.btn2_2);
            this.pnlBoard2.Controls.Add(this.btn2_3);
            this.pnlBoard2.Controls.Add(this.btn2_1);
            this.pnlBoard2.Controls.Add(this.btn2_0);
            this.pnlBoard2.Location = new System.Drawing.Point(250, 6);
            this.pnlBoard2.Name = "pnlBoard2";
            this.pnlBoard2.Size = new System.Drawing.Size(115, 122);
            this.pnlBoard2.TabIndex = 4;
            // 
            // btn2_8
            // 
            this.btn2_8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn2_8.Location = new System.Drawing.Point(78, 82);
            this.btn2_8.Name = "btn2_8";
            this.btn2_8.Size = new System.Drawing.Size(30, 32);
            this.btn2_8.TabIndex = 0;
            this.btn2_8.Text = " ";
            this.btn2_8.UseVisualStyleBackColor = false;
            this.btn2_8.Click += new System.EventHandler(this.btn2_8_Click);
            // 
            // btn2_7
            // 
            this.btn2_7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn2_7.Location = new System.Drawing.Point(42, 82);
            this.btn2_7.Name = "btn2_7";
            this.btn2_7.Size = new System.Drawing.Size(30, 32);
            this.btn2_7.TabIndex = 0;
            this.btn2_7.Text = " ";
            this.btn2_7.UseVisualStyleBackColor = false;
            this.btn2_7.Click += new System.EventHandler(this.btn2_7_Click);
            // 
            // btn2_5
            // 
            this.btn2_5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn2_5.Location = new System.Drawing.Point(78, 44);
            this.btn2_5.Name = "btn2_5";
            this.btn2_5.Size = new System.Drawing.Size(30, 32);
            this.btn2_5.TabIndex = 0;
            this.btn2_5.Text = " ";
            this.btn2_5.UseVisualStyleBackColor = false;
            this.btn2_5.Click += new System.EventHandler(this.btn2_5_Click);
            // 
            // btn2_4
            // 
            this.btn2_4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn2_4.Location = new System.Drawing.Point(42, 44);
            this.btn2_4.Name = "btn2_4";
            this.btn2_4.Size = new System.Drawing.Size(30, 32);
            this.btn2_4.TabIndex = 0;
            this.btn2_4.Text = " ";
            this.btn2_4.UseVisualStyleBackColor = false;
            this.btn2_4.Click += new System.EventHandler(this.btn2_4_Click);
            // 
            // btn2_6
            // 
            this.btn2_6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn2_6.Location = new System.Drawing.Point(6, 82);
            this.btn2_6.Name = "btn2_6";
            this.btn2_6.Size = new System.Drawing.Size(30, 32);
            this.btn2_6.TabIndex = 0;
            this.btn2_6.Text = " ";
            this.btn2_6.UseVisualStyleBackColor = false;
            this.btn2_6.Click += new System.EventHandler(this.btn2_6_Click);
            // 
            // btn2_2
            // 
            this.btn2_2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn2_2.Location = new System.Drawing.Point(78, 6);
            this.btn2_2.Name = "btn2_2";
            this.btn2_2.Size = new System.Drawing.Size(30, 32);
            this.btn2_2.TabIndex = 0;
            this.btn2_2.Text = " ";
            this.btn2_2.UseVisualStyleBackColor = false;
            this.btn2_2.Click += new System.EventHandler(this.btn2_2_Click);
            // 
            // btn2_3
            // 
            this.btn2_3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn2_3.Location = new System.Drawing.Point(6, 44);
            this.btn2_3.Name = "btn2_3";
            this.btn2_3.Size = new System.Drawing.Size(30, 32);
            this.btn2_3.TabIndex = 0;
            this.btn2_3.Text = " ";
            this.btn2_3.UseVisualStyleBackColor = false;
            this.btn2_3.Click += new System.EventHandler(this.btn2_3_Click);
            // 
            // btn2_1
            // 
            this.btn2_1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn2_1.Location = new System.Drawing.Point(42, 6);
            this.btn2_1.Name = "btn2_1";
            this.btn2_1.Size = new System.Drawing.Size(30, 32);
            this.btn2_1.TabIndex = 0;
            this.btn2_1.Text = " ";
            this.btn2_1.UseVisualStyleBackColor = false;
            this.btn2_1.Click += new System.EventHandler(this.btn2_1_Click);
            // 
            // btn2_0
            // 
            this.btn2_0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn2_0.Location = new System.Drawing.Point(6, 6);
            this.btn2_0.Name = "btn2_0";
            this.btn2_0.Size = new System.Drawing.Size(30, 32);
            this.btn2_0.TabIndex = 0;
            this.btn2_0.Text = " ";
            this.btn2_0.UseVisualStyleBackColor = false;
            this.btn2_0.Click += new System.EventHandler(this.btn2_0_Click);
            // 
            // pnlBoard7
            // 
            this.pnlBoard7.Controls.Add(this.btn7_8);
            this.pnlBoard7.Controls.Add(this.btn7_7);
            this.pnlBoard7.Controls.Add(this.btn7_5);
            this.pnlBoard7.Controls.Add(this.btn7_4);
            this.pnlBoard7.Controls.Add(this.btn7_6);
            this.pnlBoard7.Controls.Add(this.btn7_2);
            this.pnlBoard7.Controls.Add(this.btn7_3);
            this.pnlBoard7.Controls.Add(this.btn7_1);
            this.pnlBoard7.Controls.Add(this.btn7_0);
            this.pnlBoard7.Location = new System.Drawing.Point(129, 262);
            this.pnlBoard7.Name = "pnlBoard7";
            this.pnlBoard7.Size = new System.Drawing.Size(115, 122);
            this.pnlBoard7.TabIndex = 4;
            // 
            // btn7_8
            // 
            this.btn7_8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn7_8.Location = new System.Drawing.Point(78, 82);
            this.btn7_8.Name = "btn7_8";
            this.btn7_8.Size = new System.Drawing.Size(30, 32);
            this.btn7_8.TabIndex = 0;
            this.btn7_8.Text = " ";
            this.btn7_8.UseVisualStyleBackColor = false;
            this.btn7_8.Click += new System.EventHandler(this.btn7_8_Click);
            // 
            // btn7_7
            // 
            this.btn7_7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn7_7.Location = new System.Drawing.Point(42, 82);
            this.btn7_7.Name = "btn7_7";
            this.btn7_7.Size = new System.Drawing.Size(30, 32);
            this.btn7_7.TabIndex = 0;
            this.btn7_7.Text = " ";
            this.btn7_7.UseVisualStyleBackColor = false;
            this.btn7_7.Click += new System.EventHandler(this.btn7_7_Click);
            // 
            // btn7_5
            // 
            this.btn7_5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn7_5.Location = new System.Drawing.Point(78, 44);
            this.btn7_5.Name = "btn7_5";
            this.btn7_5.Size = new System.Drawing.Size(30, 32);
            this.btn7_5.TabIndex = 0;
            this.btn7_5.Text = " ";
            this.btn7_5.UseVisualStyleBackColor = false;
            this.btn7_5.Click += new System.EventHandler(this.btn7_5_Click);
            // 
            // btn7_4
            // 
            this.btn7_4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn7_4.Location = new System.Drawing.Point(42, 44);
            this.btn7_4.Name = "btn7_4";
            this.btn7_4.Size = new System.Drawing.Size(30, 32);
            this.btn7_4.TabIndex = 0;
            this.btn7_4.Text = " ";
            this.btn7_4.UseVisualStyleBackColor = false;
            this.btn7_4.Click += new System.EventHandler(this.btn7_4_Click);
            // 
            // btn7_6
            // 
            this.btn7_6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn7_6.Location = new System.Drawing.Point(6, 82);
            this.btn7_6.Name = "btn7_6";
            this.btn7_6.Size = new System.Drawing.Size(30, 32);
            this.btn7_6.TabIndex = 0;
            this.btn7_6.Text = " ";
            this.btn7_6.UseVisualStyleBackColor = false;
            this.btn7_6.Click += new System.EventHandler(this.btn7_6_Click);
            // 
            // btn7_2
            // 
            this.btn7_2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn7_2.Location = new System.Drawing.Point(78, 6);
            this.btn7_2.Name = "btn7_2";
            this.btn7_2.Size = new System.Drawing.Size(30, 32);
            this.btn7_2.TabIndex = 0;
            this.btn7_2.Text = " ";
            this.btn7_2.UseVisualStyleBackColor = false;
            this.btn7_2.Click += new System.EventHandler(this.btn7_2_Click);
            // 
            // btn7_3
            // 
            this.btn7_3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn7_3.Location = new System.Drawing.Point(6, 44);
            this.btn7_3.Name = "btn7_3";
            this.btn7_3.Size = new System.Drawing.Size(30, 32);
            this.btn7_3.TabIndex = 0;
            this.btn7_3.Text = " ";
            this.btn7_3.UseVisualStyleBackColor = false;
            this.btn7_3.Click += new System.EventHandler(this.btn7_3_Click);
            // 
            // btn7_1
            // 
            this.btn7_1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn7_1.Location = new System.Drawing.Point(42, 6);
            this.btn7_1.Name = "btn7_1";
            this.btn7_1.Size = new System.Drawing.Size(30, 32);
            this.btn7_1.TabIndex = 0;
            this.btn7_1.Text = " ";
            this.btn7_1.UseVisualStyleBackColor = false;
            this.btn7_1.Click += new System.EventHandler(this.btn7_1_Click);
            // 
            // btn7_0
            // 
            this.btn7_0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn7_0.Location = new System.Drawing.Point(6, 6);
            this.btn7_0.Name = "btn7_0";
            this.btn7_0.Size = new System.Drawing.Size(30, 32);
            this.btn7_0.TabIndex = 0;
            this.btn7_0.Text = " ";
            this.btn7_0.UseVisualStyleBackColor = false;
            this.btn7_0.Click += new System.EventHandler(this.btn7_0_Click);
            // 
            // pnlBoard4
            // 
            this.pnlBoard4.Controls.Add(this.btn4_8);
            this.pnlBoard4.Controls.Add(this.btn4_7);
            this.pnlBoard4.Controls.Add(this.btn4_5);
            this.pnlBoard4.Controls.Add(this.btn4_4);
            this.pnlBoard4.Controls.Add(this.btn4_6);
            this.pnlBoard4.Controls.Add(this.btn4_2);
            this.pnlBoard4.Controls.Add(this.btn4_3);
            this.pnlBoard4.Controls.Add(this.btn4_1);
            this.pnlBoard4.Controls.Add(this.btn4_0);
            this.pnlBoard4.Location = new System.Drawing.Point(129, 134);
            this.pnlBoard4.Name = "pnlBoard4";
            this.pnlBoard4.Size = new System.Drawing.Size(115, 122);
            this.pnlBoard4.TabIndex = 4;
            // 
            // btn4_8
            // 
            this.btn4_8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn4_8.Location = new System.Drawing.Point(78, 82);
            this.btn4_8.Name = "btn4_8";
            this.btn4_8.Size = new System.Drawing.Size(30, 32);
            this.btn4_8.TabIndex = 0;
            this.btn4_8.Text = " ";
            this.btn4_8.UseVisualStyleBackColor = false;
            this.btn4_8.Click += new System.EventHandler(this.btn4_8_Click);
            // 
            // btn4_7
            // 
            this.btn4_7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn4_7.Location = new System.Drawing.Point(42, 82);
            this.btn4_7.Name = "btn4_7";
            this.btn4_7.Size = new System.Drawing.Size(30, 32);
            this.btn4_7.TabIndex = 0;
            this.btn4_7.Text = " ";
            this.btn4_7.UseVisualStyleBackColor = false;
            this.btn4_7.Click += new System.EventHandler(this.btn4_7_Click);
            // 
            // btn4_5
            // 
            this.btn4_5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn4_5.Location = new System.Drawing.Point(78, 44);
            this.btn4_5.Name = "btn4_5";
            this.btn4_5.Size = new System.Drawing.Size(30, 32);
            this.btn4_5.TabIndex = 0;
            this.btn4_5.Text = " ";
            this.btn4_5.UseVisualStyleBackColor = false;
            this.btn4_5.Click += new System.EventHandler(this.btn4_5_Click);
            // 
            // btn4_4
            // 
            this.btn4_4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn4_4.Location = new System.Drawing.Point(42, 44);
            this.btn4_4.Name = "btn4_4";
            this.btn4_4.Size = new System.Drawing.Size(30, 32);
            this.btn4_4.TabIndex = 0;
            this.btn4_4.Text = " ";
            this.btn4_4.UseVisualStyleBackColor = false;
            this.btn4_4.Click += new System.EventHandler(this.btn4_4_Click);
            // 
            // btn4_6
            // 
            this.btn4_6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn4_6.Location = new System.Drawing.Point(6, 82);
            this.btn4_6.Name = "btn4_6";
            this.btn4_6.Size = new System.Drawing.Size(30, 32);
            this.btn4_6.TabIndex = 0;
            this.btn4_6.Text = " ";
            this.btn4_6.UseVisualStyleBackColor = false;
            this.btn4_6.Click += new System.EventHandler(this.btn4_6_Click);
            // 
            // btn4_2
            // 
            this.btn4_2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn4_2.Location = new System.Drawing.Point(78, 6);
            this.btn4_2.Name = "btn4_2";
            this.btn4_2.Size = new System.Drawing.Size(30, 32);
            this.btn4_2.TabIndex = 0;
            this.btn4_2.Text = " ";
            this.btn4_2.UseVisualStyleBackColor = false;
            this.btn4_2.Click += new System.EventHandler(this.btn4_2_Click);
            // 
            // btn4_3
            // 
            this.btn4_3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn4_3.Location = new System.Drawing.Point(6, 44);
            this.btn4_3.Name = "btn4_3";
            this.btn4_3.Size = new System.Drawing.Size(30, 32);
            this.btn4_3.TabIndex = 0;
            this.btn4_3.Text = " ";
            this.btn4_3.UseVisualStyleBackColor = false;
            this.btn4_3.Click += new System.EventHandler(this.btn4_3_Click);
            // 
            // btn4_1
            // 
            this.btn4_1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn4_1.Location = new System.Drawing.Point(42, 6);
            this.btn4_1.Name = "btn4_1";
            this.btn4_1.Size = new System.Drawing.Size(30, 32);
            this.btn4_1.TabIndex = 0;
            this.btn4_1.Text = " ";
            this.btn4_1.UseVisualStyleBackColor = false;
            this.btn4_1.Click += new System.EventHandler(this.btn4_1_Click);
            // 
            // btn4_0
            // 
            this.btn4_0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn4_0.Location = new System.Drawing.Point(6, 6);
            this.btn4_0.Name = "btn4_0";
            this.btn4_0.Size = new System.Drawing.Size(30, 32);
            this.btn4_0.TabIndex = 0;
            this.btn4_0.Text = " ";
            this.btn4_0.UseVisualStyleBackColor = false;
            this.btn4_0.Click += new System.EventHandler(this.btn4_0_Click);
            // 
            // pnlBoard1
            // 
            this.pnlBoard1.Controls.Add(this.btn1_8);
            this.pnlBoard1.Controls.Add(this.btn1_7);
            this.pnlBoard1.Controls.Add(this.btn1_5);
            this.pnlBoard1.Controls.Add(this.btn1_4);
            this.pnlBoard1.Controls.Add(this.btn1_6);
            this.pnlBoard1.Controls.Add(this.btn1_2);
            this.pnlBoard1.Controls.Add(this.btn1_3);
            this.pnlBoard1.Controls.Add(this.btn1_1);
            this.pnlBoard1.Controls.Add(this.btn1_0);
            this.pnlBoard1.Location = new System.Drawing.Point(129, 6);
            this.pnlBoard1.Name = "pnlBoard1";
            this.pnlBoard1.Size = new System.Drawing.Size(115, 122);
            this.pnlBoard1.TabIndex = 4;
            // 
            // btn1_8
            // 
            this.btn1_8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn1_8.Location = new System.Drawing.Point(78, 82);
            this.btn1_8.Name = "btn1_8";
            this.btn1_8.Size = new System.Drawing.Size(30, 32);
            this.btn1_8.TabIndex = 0;
            this.btn1_8.Text = " ";
            this.btn1_8.UseVisualStyleBackColor = false;
            this.btn1_8.Click += new System.EventHandler(this.btn1_8_Click);
            // 
            // btn1_7
            // 
            this.btn1_7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn1_7.Location = new System.Drawing.Point(42, 82);
            this.btn1_7.Name = "btn1_7";
            this.btn1_7.Size = new System.Drawing.Size(30, 32);
            this.btn1_7.TabIndex = 0;
            this.btn1_7.Text = " ";
            this.btn1_7.UseVisualStyleBackColor = false;
            this.btn1_7.Click += new System.EventHandler(this.btn1_7_Click);
            // 
            // btn1_5
            // 
            this.btn1_5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn1_5.Location = new System.Drawing.Point(78, 44);
            this.btn1_5.Name = "btn1_5";
            this.btn1_5.Size = new System.Drawing.Size(30, 32);
            this.btn1_5.TabIndex = 0;
            this.btn1_5.Text = " ";
            this.btn1_5.UseVisualStyleBackColor = false;
            this.btn1_5.Click += new System.EventHandler(this.btn1_5_Click);
            // 
            // btn1_4
            // 
            this.btn1_4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn1_4.Location = new System.Drawing.Point(42, 44);
            this.btn1_4.Name = "btn1_4";
            this.btn1_4.Size = new System.Drawing.Size(30, 32);
            this.btn1_4.TabIndex = 0;
            this.btn1_4.Text = " ";
            this.btn1_4.UseVisualStyleBackColor = false;
            this.btn1_4.Click += new System.EventHandler(this.btn1_4_Click);
            // 
            // btn1_6
            // 
            this.btn1_6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn1_6.Location = new System.Drawing.Point(6, 82);
            this.btn1_6.Name = "btn1_6";
            this.btn1_6.Size = new System.Drawing.Size(30, 32);
            this.btn1_6.TabIndex = 0;
            this.btn1_6.Text = " ";
            this.btn1_6.UseVisualStyleBackColor = false;
            this.btn1_6.Click += new System.EventHandler(this.btn1_6_Click);
            // 
            // btn1_2
            // 
            this.btn1_2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn1_2.Location = new System.Drawing.Point(78, 6);
            this.btn1_2.Name = "btn1_2";
            this.btn1_2.Size = new System.Drawing.Size(30, 32);
            this.btn1_2.TabIndex = 0;
            this.btn1_2.Text = " ";
            this.btn1_2.UseVisualStyleBackColor = false;
            this.btn1_2.Click += new System.EventHandler(this.btn1_2_Click);
            // 
            // btn1_3
            // 
            this.btn1_3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn1_3.Location = new System.Drawing.Point(6, 44);
            this.btn1_3.Name = "btn1_3";
            this.btn1_3.Size = new System.Drawing.Size(30, 32);
            this.btn1_3.TabIndex = 0;
            this.btn1_3.Text = " ";
            this.btn1_3.UseVisualStyleBackColor = false;
            this.btn1_3.Click += new System.EventHandler(this.btn1_3_Click);
            // 
            // btn1_1
            // 
            this.btn1_1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn1_1.Location = new System.Drawing.Point(42, 6);
            this.btn1_1.Name = "btn1_1";
            this.btn1_1.Size = new System.Drawing.Size(30, 32);
            this.btn1_1.TabIndex = 0;
            this.btn1_1.Text = " ";
            this.btn1_1.UseVisualStyleBackColor = false;
            this.btn1_1.Click += new System.EventHandler(this.btn1_1_Click);
            // 
            // btn1_0
            // 
            this.btn1_0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn1_0.Location = new System.Drawing.Point(6, 6);
            this.btn1_0.Name = "btn1_0";
            this.btn1_0.Size = new System.Drawing.Size(30, 32);
            this.btn1_0.TabIndex = 0;
            this.btn1_0.Text = " ";
            this.btn1_0.UseVisualStyleBackColor = false;
            this.btn1_0.Click += new System.EventHandler(this.btn1_0_Click);
            // 
            // pnlBoard6
            // 
            this.pnlBoard6.Controls.Add(this.btn6_8);
            this.pnlBoard6.Controls.Add(this.btn6_7);
            this.pnlBoard6.Controls.Add(this.btn6_5);
            this.pnlBoard6.Controls.Add(this.btn6_4);
            this.pnlBoard6.Controls.Add(this.btn6_6);
            this.pnlBoard6.Controls.Add(this.btn6_2);
            this.pnlBoard6.Controls.Add(this.btn6_3);
            this.pnlBoard6.Controls.Add(this.btn6_1);
            this.pnlBoard6.Controls.Add(this.btn6_0);
            this.pnlBoard6.Location = new System.Drawing.Point(8, 262);
            this.pnlBoard6.Name = "pnlBoard6";
            this.pnlBoard6.Size = new System.Drawing.Size(115, 122);
            this.pnlBoard6.TabIndex = 4;
            // 
            // btn6_8
            // 
            this.btn6_8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn6_8.Location = new System.Drawing.Point(78, 82);
            this.btn6_8.Name = "btn6_8";
            this.btn6_8.Size = new System.Drawing.Size(30, 32);
            this.btn6_8.TabIndex = 0;
            this.btn6_8.Text = " ";
            this.btn6_8.UseVisualStyleBackColor = false;
            this.btn6_8.Click += new System.EventHandler(this.btn6_8_Click);
            // 
            // btn6_7
            // 
            this.btn6_7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn6_7.Location = new System.Drawing.Point(42, 82);
            this.btn6_7.Name = "btn6_7";
            this.btn6_7.Size = new System.Drawing.Size(30, 32);
            this.btn6_7.TabIndex = 0;
            this.btn6_7.Text = " ";
            this.btn6_7.UseVisualStyleBackColor = false;
            this.btn6_7.Click += new System.EventHandler(this.btn6_7_Click);
            // 
            // btn6_5
            // 
            this.btn6_5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn6_5.Location = new System.Drawing.Point(78, 44);
            this.btn6_5.Name = "btn6_5";
            this.btn6_5.Size = new System.Drawing.Size(30, 32);
            this.btn6_5.TabIndex = 0;
            this.btn6_5.Text = " ";
            this.btn6_5.UseVisualStyleBackColor = false;
            this.btn6_5.Click += new System.EventHandler(this.btn6_5_Click);
            // 
            // btn6_4
            // 
            this.btn6_4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn6_4.Location = new System.Drawing.Point(42, 44);
            this.btn6_4.Name = "btn6_4";
            this.btn6_4.Size = new System.Drawing.Size(30, 32);
            this.btn6_4.TabIndex = 0;
            this.btn6_4.Text = " ";
            this.btn6_4.UseVisualStyleBackColor = false;
            this.btn6_4.Click += new System.EventHandler(this.btn6_4_Click);
            // 
            // btn6_6
            // 
            this.btn6_6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn6_6.Location = new System.Drawing.Point(6, 82);
            this.btn6_6.Name = "btn6_6";
            this.btn6_6.Size = new System.Drawing.Size(30, 32);
            this.btn6_6.TabIndex = 0;
            this.btn6_6.Text = " ";
            this.btn6_6.UseVisualStyleBackColor = false;
            this.btn6_6.Click += new System.EventHandler(this.btn6_6_Click);
            // 
            // btn6_2
            // 
            this.btn6_2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn6_2.Location = new System.Drawing.Point(78, 6);
            this.btn6_2.Name = "btn6_2";
            this.btn6_2.Size = new System.Drawing.Size(30, 32);
            this.btn6_2.TabIndex = 0;
            this.btn6_2.Text = " ";
            this.btn6_2.UseVisualStyleBackColor = false;
            this.btn6_2.Click += new System.EventHandler(this.btn6_2_Click);
            // 
            // btn6_3
            // 
            this.btn6_3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn6_3.Location = new System.Drawing.Point(6, 44);
            this.btn6_3.Name = "btn6_3";
            this.btn6_3.Size = new System.Drawing.Size(30, 32);
            this.btn6_3.TabIndex = 0;
            this.btn6_3.Text = " ";
            this.btn6_3.UseVisualStyleBackColor = false;
            this.btn6_3.Click += new System.EventHandler(this.btn6_3_Click);
            // 
            // btn6_1
            // 
            this.btn6_1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn6_1.Location = new System.Drawing.Point(42, 6);
            this.btn6_1.Name = "btn6_1";
            this.btn6_1.Size = new System.Drawing.Size(30, 32);
            this.btn6_1.TabIndex = 0;
            this.btn6_1.Text = " ";
            this.btn6_1.UseVisualStyleBackColor = false;
            this.btn6_1.Click += new System.EventHandler(this.btn6_1_Click);
            // 
            // btn6_0
            // 
            this.btn6_0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn6_0.Location = new System.Drawing.Point(6, 6);
            this.btn6_0.Name = "btn6_0";
            this.btn6_0.Size = new System.Drawing.Size(30, 32);
            this.btn6_0.TabIndex = 0;
            this.btn6_0.Text = " ";
            this.btn6_0.UseVisualStyleBackColor = false;
            this.btn6_0.Click += new System.EventHandler(this.btn6_0_Click);
            // 
            // pnlBoard3
            // 
            this.pnlBoard3.Controls.Add(this.btn3_8);
            this.pnlBoard3.Controls.Add(this.btn3_7);
            this.pnlBoard3.Controls.Add(this.btn3_5);
            this.pnlBoard3.Controls.Add(this.btn3_4);
            this.pnlBoard3.Controls.Add(this.btn3_6);
            this.pnlBoard3.Controls.Add(this.btn3_2);
            this.pnlBoard3.Controls.Add(this.btn3_3);
            this.pnlBoard3.Controls.Add(this.btn3_1);
            this.pnlBoard3.Controls.Add(this.btn3_0);
            this.pnlBoard3.Location = new System.Drawing.Point(8, 134);
            this.pnlBoard3.Name = "pnlBoard3";
            this.pnlBoard3.Size = new System.Drawing.Size(115, 122);
            this.pnlBoard3.TabIndex = 4;
            // 
            // btn3_8
            // 
            this.btn3_8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn3_8.Location = new System.Drawing.Point(78, 82);
            this.btn3_8.Name = "btn3_8";
            this.btn3_8.Size = new System.Drawing.Size(30, 32);
            this.btn3_8.TabIndex = 0;
            this.btn3_8.Text = " ";
            this.btn3_8.UseVisualStyleBackColor = false;
            this.btn3_8.Click += new System.EventHandler(this.btn3_8_Click);
            // 
            // btn3_7
            // 
            this.btn3_7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn3_7.Location = new System.Drawing.Point(42, 82);
            this.btn3_7.Name = "btn3_7";
            this.btn3_7.Size = new System.Drawing.Size(30, 32);
            this.btn3_7.TabIndex = 0;
            this.btn3_7.Text = " ";
            this.btn3_7.UseVisualStyleBackColor = false;
            this.btn3_7.Click += new System.EventHandler(this.btn3_7_Click);
            // 
            // btn3_5
            // 
            this.btn3_5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn3_5.Location = new System.Drawing.Point(78, 44);
            this.btn3_5.Name = "btn3_5";
            this.btn3_5.Size = new System.Drawing.Size(30, 32);
            this.btn3_5.TabIndex = 0;
            this.btn3_5.Text = " ";
            this.btn3_5.UseVisualStyleBackColor = false;
            this.btn3_5.Click += new System.EventHandler(this.btn3_5_Click);
            // 
            // btn3_4
            // 
            this.btn3_4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn3_4.Location = new System.Drawing.Point(42, 44);
            this.btn3_4.Name = "btn3_4";
            this.btn3_4.Size = new System.Drawing.Size(30, 32);
            this.btn3_4.TabIndex = 0;
            this.btn3_4.Text = " ";
            this.btn3_4.UseVisualStyleBackColor = false;
            this.btn3_4.Click += new System.EventHandler(this.btn3_4_Click);
            // 
            // btn3_6
            // 
            this.btn3_6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn3_6.Location = new System.Drawing.Point(6, 82);
            this.btn3_6.Name = "btn3_6";
            this.btn3_6.Size = new System.Drawing.Size(30, 32);
            this.btn3_6.TabIndex = 0;
            this.btn3_6.Text = " ";
            this.btn3_6.UseVisualStyleBackColor = false;
            this.btn3_6.Click += new System.EventHandler(this.btn3_6_Click);
            // 
            // btn3_2
            // 
            this.btn3_2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn3_2.Location = new System.Drawing.Point(78, 6);
            this.btn3_2.Name = "btn3_2";
            this.btn3_2.Size = new System.Drawing.Size(30, 32);
            this.btn3_2.TabIndex = 0;
            this.btn3_2.Text = " ";
            this.btn3_2.UseVisualStyleBackColor = false;
            this.btn3_2.Click += new System.EventHandler(this.btn3_2_Click);
            // 
            // btn3_3
            // 
            this.btn3_3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn3_3.Location = new System.Drawing.Point(6, 44);
            this.btn3_3.Name = "btn3_3";
            this.btn3_3.Size = new System.Drawing.Size(30, 32);
            this.btn3_3.TabIndex = 0;
            this.btn3_3.Text = " ";
            this.btn3_3.UseVisualStyleBackColor = false;
            this.btn3_3.Click += new System.EventHandler(this.btn3_3_Click);
            // 
            // btn3_1
            // 
            this.btn3_1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn3_1.Location = new System.Drawing.Point(42, 6);
            this.btn3_1.Name = "btn3_1";
            this.btn3_1.Size = new System.Drawing.Size(30, 32);
            this.btn3_1.TabIndex = 0;
            this.btn3_1.Text = " ";
            this.btn3_1.UseVisualStyleBackColor = false;
            this.btn3_1.Click += new System.EventHandler(this.btn3_1_Click);
            // 
            // btn3_0
            // 
            this.btn3_0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn3_0.Location = new System.Drawing.Point(6, 6);
            this.btn3_0.Name = "btn3_0";
            this.btn3_0.Size = new System.Drawing.Size(30, 32);
            this.btn3_0.TabIndex = 0;
            this.btn3_0.Text = " ";
            this.btn3_0.UseVisualStyleBackColor = false;
            this.btn3_0.Click += new System.EventHandler(this.btn3_0_Click);
            // 
            // pnlBoard0
            // 
            this.pnlBoard0.BackColor = System.Drawing.Color.Transparent;
            this.pnlBoard0.Controls.Add(this.btn0_8);
            this.pnlBoard0.Controls.Add(this.btn0_7);
            this.pnlBoard0.Controls.Add(this.btn0_5);
            this.pnlBoard0.Controls.Add(this.btn0_4);
            this.pnlBoard0.Controls.Add(this.btn0_6);
            this.pnlBoard0.Controls.Add(this.btn0_2);
            this.pnlBoard0.Controls.Add(this.btn0_3);
            this.pnlBoard0.Controls.Add(this.btn0_1);
            this.pnlBoard0.Controls.Add(this.btn0_0);
            this.pnlBoard0.Location = new System.Drawing.Point(8, 6);
            this.pnlBoard0.Name = "pnlBoard0";
            this.pnlBoard0.Size = new System.Drawing.Size(115, 122);
            this.pnlBoard0.TabIndex = 4;
            // 
            // btn0_8
            // 
            this.btn0_8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn0_8.Location = new System.Drawing.Point(78, 82);
            this.btn0_8.Name = "btn0_8";
            this.btn0_8.Size = new System.Drawing.Size(30, 32);
            this.btn0_8.TabIndex = 0;
            this.btn0_8.Text = " ";
            this.btn0_8.UseVisualStyleBackColor = false;
            this.btn0_8.Click += new System.EventHandler(this.btn0_8_Click);
            // 
            // btn0_7
            // 
            this.btn0_7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn0_7.Location = new System.Drawing.Point(42, 82);
            this.btn0_7.Name = "btn0_7";
            this.btn0_7.Size = new System.Drawing.Size(30, 32);
            this.btn0_7.TabIndex = 0;
            this.btn0_7.Text = " ";
            this.btn0_7.UseVisualStyleBackColor = false;
            this.btn0_7.Click += new System.EventHandler(this.btn0_7_Click);
            // 
            // btn0_5
            // 
            this.btn0_5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn0_5.Location = new System.Drawing.Point(78, 44);
            this.btn0_5.Name = "btn0_5";
            this.btn0_5.Size = new System.Drawing.Size(30, 32);
            this.btn0_5.TabIndex = 0;
            this.btn0_5.Text = " ";
            this.btn0_5.UseVisualStyleBackColor = false;
            this.btn0_5.Click += new System.EventHandler(this.btn0_5_Click);
            // 
            // btn0_4
            // 
            this.btn0_4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn0_4.Location = new System.Drawing.Point(42, 44);
            this.btn0_4.Name = "btn0_4";
            this.btn0_4.Size = new System.Drawing.Size(30, 32);
            this.btn0_4.TabIndex = 0;
            this.btn0_4.Text = " ";
            this.btn0_4.UseVisualStyleBackColor = false;
            this.btn0_4.Click += new System.EventHandler(this.btn0_4_Click);
            // 
            // btn0_6
            // 
            this.btn0_6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn0_6.Location = new System.Drawing.Point(6, 82);
            this.btn0_6.Name = "btn0_6";
            this.btn0_6.Size = new System.Drawing.Size(30, 32);
            this.btn0_6.TabIndex = 0;
            this.btn0_6.Text = " ";
            this.btn0_6.UseVisualStyleBackColor = false;
            this.btn0_6.Click += new System.EventHandler(this.btn0_6_Click);
            // 
            // btn0_2
            // 
            this.btn0_2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn0_2.Location = new System.Drawing.Point(78, 6);
            this.btn0_2.Name = "btn0_2";
            this.btn0_2.Size = new System.Drawing.Size(30, 32);
            this.btn0_2.TabIndex = 0;
            this.btn0_2.Text = " ";
            this.btn0_2.UseVisualStyleBackColor = false;
            this.btn0_2.Click += new System.EventHandler(this.btn0_2_Click);
            // 
            // btn0_3
            // 
            this.btn0_3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn0_3.Location = new System.Drawing.Point(6, 44);
            this.btn0_3.Name = "btn0_3";
            this.btn0_3.Size = new System.Drawing.Size(30, 32);
            this.btn0_3.TabIndex = 0;
            this.btn0_3.Text = " ";
            this.btn0_3.UseVisualStyleBackColor = false;
            this.btn0_3.Click += new System.EventHandler(this.btn0_3_Click);
            // 
            // btn0_1
            // 
            this.btn0_1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn0_1.Location = new System.Drawing.Point(42, 6);
            this.btn0_1.Name = "btn0_1";
            this.btn0_1.Size = new System.Drawing.Size(30, 32);
            this.btn0_1.TabIndex = 0;
            this.btn0_1.Text = " ";
            this.btn0_1.UseVisualStyleBackColor = false;
            this.btn0_1.Click += new System.EventHandler(this.btn0_1_Click);
            // 
            // btn0_0
            // 
            this.btn0_0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn0_0.Location = new System.Drawing.Point(6, 6);
            this.btn0_0.Name = "btn0_0";
            this.btn0_0.Size = new System.Drawing.Size(30, 32);
            this.btn0_0.TabIndex = 0;
            this.btn0_0.UseVisualStyleBackColor = false;
            this.btn0_0.Click += new System.EventHandler(this.btn0_0_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.nudNumber);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.labStatus);
            this.tabPage1.Controls.Add(this.panel16);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.BtnReset);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.BtnGo);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(535, 391);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "3х3";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(358, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Tiny board number:";
            // 
            // nudNumber
            // 
            this.nudNumber.Location = new System.Drawing.Point(476, 49);
            this.nudNumber.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.nudNumber.Name = "nudNumber";
            this.nudNumber.Size = new System.Drawing.Size(51, 20);
            this.nudNumber.TabIndex = 7;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.tbWeight8);
            this.panel16.Controls.Add(this.tbWeight7);
            this.panel16.Controls.Add(this.tbWeight5);
            this.panel16.Controls.Add(this.tbWeight4);
            this.panel16.Controls.Add(this.tbWeight6);
            this.panel16.Controls.Add(this.tbWeight2);
            this.panel16.Controls.Add(this.tbWeight3);
            this.panel16.Controls.Add(this.tbWeight1);
            this.panel16.Controls.Add(this.tbWeight0);
            this.panel16.Location = new System.Drawing.Point(182, 6);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(170, 169);
            this.panel16.TabIndex = 2;
            // 
            // tbWeight8
            // 
            this.tbWeight8.Location = new System.Drawing.Point(111, 112);
            this.tbWeight8.Multiline = true;
            this.tbWeight8.Name = "tbWeight8";
            this.tbWeight8.Size = new System.Drawing.Size(43, 43);
            this.tbWeight8.TabIndex = 0;
            this.tbWeight8.Text = "1.0";
            // 
            // tbWeight7
            // 
            this.tbWeight7.Location = new System.Drawing.Point(62, 112);
            this.tbWeight7.Multiline = true;
            this.tbWeight7.Name = "tbWeight7";
            this.tbWeight7.Size = new System.Drawing.Size(43, 43);
            this.tbWeight7.TabIndex = 0;
            this.tbWeight7.Text = "1.0";
            // 
            // tbWeight5
            // 
            this.tbWeight5.Location = new System.Drawing.Point(111, 63);
            this.tbWeight5.Multiline = true;
            this.tbWeight5.Name = "tbWeight5";
            this.tbWeight5.Size = new System.Drawing.Size(43, 43);
            this.tbWeight5.TabIndex = 0;
            this.tbWeight5.Text = "1.0";
            // 
            // tbWeight4
            // 
            this.tbWeight4.Location = new System.Drawing.Point(62, 63);
            this.tbWeight4.Multiline = true;
            this.tbWeight4.Name = "tbWeight4";
            this.tbWeight4.Size = new System.Drawing.Size(43, 43);
            this.tbWeight4.TabIndex = 0;
            this.tbWeight4.Text = "1.0";
            // 
            // tbWeight6
            // 
            this.tbWeight6.Location = new System.Drawing.Point(13, 112);
            this.tbWeight6.Multiline = true;
            this.tbWeight6.Name = "tbWeight6";
            this.tbWeight6.Size = new System.Drawing.Size(43, 43);
            this.tbWeight6.TabIndex = 0;
            this.tbWeight6.Text = "1.0";
            // 
            // tbWeight2
            // 
            this.tbWeight2.Location = new System.Drawing.Point(111, 14);
            this.tbWeight2.Multiline = true;
            this.tbWeight2.Name = "tbWeight2";
            this.tbWeight2.Size = new System.Drawing.Size(43, 43);
            this.tbWeight2.TabIndex = 0;
            this.tbWeight2.Text = "1.0";
            // 
            // tbWeight3
            // 
            this.tbWeight3.Location = new System.Drawing.Point(13, 63);
            this.tbWeight3.Multiline = true;
            this.tbWeight3.Name = "tbWeight3";
            this.tbWeight3.Size = new System.Drawing.Size(43, 43);
            this.tbWeight3.TabIndex = 0;
            this.tbWeight3.Text = "1.0";
            // 
            // tbWeight1
            // 
            this.tbWeight1.Location = new System.Drawing.Point(62, 14);
            this.tbWeight1.Multiline = true;
            this.tbWeight1.Name = "tbWeight1";
            this.tbWeight1.Size = new System.Drawing.Size(43, 43);
            this.tbWeight1.TabIndex = 0;
            this.tbWeight1.Text = "1.0";
            // 
            // tbWeight0
            // 
            this.tbWeight0.Location = new System.Drawing.Point(13, 14);
            this.tbWeight0.Multiline = true;
            this.tbWeight0.Name = "tbWeight0";
            this.tbWeight0.Size = new System.Drawing.Size(43, 43);
            this.tbWeight0.TabIndex = 0;
            this.tbWeight0.Text = "1.0";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hideToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(543, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // hideToolStripMenuItem
            // 
            this.hideToolStripMenuItem.Name = "hideToolStripMenuItem";
            this.hideToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.hideToolStripMenuItem.Text = "Hide";
            this.hideToolStripMenuItem.Click += new System.EventHandler(this.hideToolStripMenuItem_Click);
            // 
            // DebugUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 417);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "DebugUI";
            this.Text = "DebugUI";
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.pnlStrategy.ResumeLayout(false);
            this.pnlTotal.ResumeLayout(false);
            this.pnlBoard8.ResumeLayout(false);
            this.pnlBoard5.ResumeLayout(false);
            this.pnlBoard2.ResumeLayout(false);
            this.pnlBoard7.ResumeLayout(false);
            this.pnlBoard4.ResumeLayout(false);
            this.pnlBoard1.ResumeLayout(false);
            this.pnlBoard6.ResumeLayout(false);
            this.pnlBoard3.ResumeLayout(false);
            this.pnlBoard0.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumber)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnReset;
        private System.Windows.Forms.Button BtnGo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lab8;
        private System.Windows.Forms.Label lab5;
        private System.Windows.Forms.Label lab2;
        private System.Windows.Forms.Label lab7;
        private System.Windows.Forms.Label lab4;
        private System.Windows.Forms.Label lab1;
        private System.Windows.Forms.Label lab6;
        private System.Windows.Forms.Label lab3;
        private System.Windows.Forms.Label lab0;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Label labStatus;
        private System.Windows.Forms.Label lab9;
        private System.Windows.Forms.Label lab12;
        private System.Windows.Forms.Label lab15;
        private System.Windows.Forms.Label lab10;
        private System.Windows.Forms.Label lab13;
        private System.Windows.Forms.Label lab16;
        private System.Windows.Forms.Label lab11;
        private System.Windows.Forms.Label lab14;
        private System.Windows.Forms.Label lab17;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel pnlBoard8;
        private System.Windows.Forms.Button btn8_8;
        private System.Windows.Forms.Button btn8_7;
        private System.Windows.Forms.Button btn8_5;
        private System.Windows.Forms.Button btn8_4;
        private System.Windows.Forms.Button btn8_6;
        private System.Windows.Forms.Button btn8_2;
        private System.Windows.Forms.Button btn8_3;
        private System.Windows.Forms.Button btn8_1;
        private System.Windows.Forms.Button btn8_0;
        private System.Windows.Forms.Panel pnlBoard5;
        private System.Windows.Forms.Button btn5_8;
        private System.Windows.Forms.Button btn5_7;
        private System.Windows.Forms.Button btn5_5;
        private System.Windows.Forms.Button btn5_4;
        private System.Windows.Forms.Button btn5_6;
        private System.Windows.Forms.Button btn5_2;
        private System.Windows.Forms.Button btn5_3;
        private System.Windows.Forms.Button btn5_1;
        private System.Windows.Forms.Button btn5_0;
        private System.Windows.Forms.Panel pnlBoard2;
        private System.Windows.Forms.Button btn2_8;
        private System.Windows.Forms.Button btn2_7;
        private System.Windows.Forms.Button btn2_5;
        private System.Windows.Forms.Button btn2_4;
        private System.Windows.Forms.Button btn2_6;
        private System.Windows.Forms.Button btn2_2;
        private System.Windows.Forms.Button btn2_3;
        private System.Windows.Forms.Button btn2_1;
        private System.Windows.Forms.Button btn2_0;
        private System.Windows.Forms.Panel pnlBoard7;
        private System.Windows.Forms.Button btn7_8;
        private System.Windows.Forms.Button btn7_7;
        private System.Windows.Forms.Button btn7_5;
        private System.Windows.Forms.Button btn7_4;
        private System.Windows.Forms.Button btn7_6;
        private System.Windows.Forms.Button btn7_2;
        private System.Windows.Forms.Button btn7_3;
        private System.Windows.Forms.Button btn7_1;
        private System.Windows.Forms.Button btn7_0;
        private System.Windows.Forms.Panel pnlBoard4;
        private System.Windows.Forms.Button btn4_8;
        private System.Windows.Forms.Button btn4_7;
        private System.Windows.Forms.Button btn4_5;
        private System.Windows.Forms.Button btn4_4;
        private System.Windows.Forms.Button btn4_6;
        private System.Windows.Forms.Button btn4_2;
        private System.Windows.Forms.Button btn4_3;
        private System.Windows.Forms.Button btn4_1;
        private System.Windows.Forms.Button btn4_0;
        private System.Windows.Forms.Panel pnlBoard1;
        private System.Windows.Forms.Button btn1_8;
        private System.Windows.Forms.Button btn1_7;
        private System.Windows.Forms.Button btn1_5;
        private System.Windows.Forms.Button btn1_4;
        private System.Windows.Forms.Button btn1_6;
        private System.Windows.Forms.Button btn1_2;
        private System.Windows.Forms.Button btn1_3;
        private System.Windows.Forms.Button btn1_1;
        private System.Windows.Forms.Button btn1_0;
        private System.Windows.Forms.Panel pnlBoard6;
        private System.Windows.Forms.Button btn6_8;
        private System.Windows.Forms.Button btn6_7;
        private System.Windows.Forms.Button btn6_5;
        private System.Windows.Forms.Button btn6_4;
        private System.Windows.Forms.Button btn6_6;
        private System.Windows.Forms.Button btn6_2;
        private System.Windows.Forms.Button btn6_3;
        private System.Windows.Forms.Button btn6_1;
        private System.Windows.Forms.Button btn6_0;
        private System.Windows.Forms.Panel pnlBoard3;
        private System.Windows.Forms.Button btn3_8;
        private System.Windows.Forms.Button btn3_7;
        private System.Windows.Forms.Button btn3_5;
        private System.Windows.Forms.Button btn3_4;
        private System.Windows.Forms.Button btn3_6;
        private System.Windows.Forms.Button btn3_2;
        private System.Windows.Forms.Button btn3_3;
        private System.Windows.Forms.Button btn3_1;
        private System.Windows.Forms.Button btn3_0;
        private System.Windows.Forms.Panel pnlBoard0;
        private System.Windows.Forms.Button btn0_8;
        private System.Windows.Forms.Button btn0_7;
        private System.Windows.Forms.Button btn0_5;
        private System.Windows.Forms.Button btn0_4;
        private System.Windows.Forms.Button btn0_6;
        private System.Windows.Forms.Button btn0_2;
        private System.Windows.Forms.Button btn0_3;
        private System.Windows.Forms.Button btn0_1;
        private System.Windows.Forms.Button btn0_0;
        private System.Windows.Forms.Panel pnlStrategy;
        private System.Windows.Forms.Label lab1_8;
        private System.Windows.Forms.Label lab1_5;
        private System.Windows.Forms.Label lab1_2;
        private System.Windows.Forms.Label lab1_7;
        private System.Windows.Forms.Label lab1_4;
        private System.Windows.Forms.Label lab1_1;
        private System.Windows.Forms.Label lab1_6;
        private System.Windows.Forms.Label lab1_3;
        private System.Windows.Forms.Label lab1_0;
        private System.Windows.Forms.Panel pnlTotal;
        private System.Windows.Forms.Label lab0_8;
        private System.Windows.Forms.Label lab0_5;
        private System.Windows.Forms.Label lab0_2;
        private System.Windows.Forms.Label lab0_7;
        private System.Windows.Forms.Label lab0_4;
        private System.Windows.Forms.Label lab0_1;
        private System.Windows.Forms.Label lab0_6;
        private System.Windows.Forms.Label lab0_3;
        private System.Windows.Forms.Label lab0_0;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.TextBox tbWeight8;
        private System.Windows.Forms.TextBox tbWeight7;
        private System.Windows.Forms.TextBox tbWeight5;
        private System.Windows.Forms.TextBox tbWeight4;
        private System.Windows.Forms.TextBox tbWeight6;
        private System.Windows.Forms.TextBox tbWeight2;
        private System.Windows.Forms.TextBox tbWeight3;
        private System.Windows.Forms.TextBox tbWeight1;
        private System.Windows.Forms.TextBox tbWeight0;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudNumber;
        private System.Windows.Forms.Button GeneticBtn;
        private System.Windows.Forms.Button ManualBtn;
        private System.Windows.Forms.Label labTotal;
        private System.Windows.Forms.Label labStrategy;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hideToolStripMenuItem;
    }
}
#endif