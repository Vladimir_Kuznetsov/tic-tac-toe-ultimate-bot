﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIGames.UltimateTicTacToe.StarterBot.Communication;

namespace AIGames.UltimateTicTacToe.StarterBot.Evolution
{
    public class Creature : ICloneable
    {
        public IBot bot;
        public float points;

        public Creature (LimitedVal[] args)
        {
            bot = new Starter(args);
        }

        public object Clone()
        {
            var newObj = new Creature(Utils.ArrayDeepCopy<LimitedVal>(((Starter)this.bot).args));
            newObj.bot = this.bot.Clone() as Starter;
            newObj.points = this.points;

            return newObj;
        }

        public void RandomizeGenes()
        {
            for (int i = 0; i < ((Starter)bot).args.Length; i++)
                ((Starter)bot).args[i].SetRandom();
            ((Starter)bot).UseArgs();
        }

        public void SetName(string name)
        {
            char[] splitters = new char[] { ' ' };
            name = name.Replace(".", ",").Replace("[ ", "").Replace(" ]", "");
            float[] argValues = name.Split(splitters).Select<string, float>(s =>
            {
                float f;
                if (float.TryParse(s, out f)) return f;
                else throw new Exception("Bad name.");
            }).ToArray<float>();

            if (argValues.Length != ((Starter)bot).args.Length)
                throw new Exception("Bad name.");

            for (int i = 0; i < ((Starter)bot).args.Length; i++)
            {
                ((Starter)bot).args[i].val = argValues[i];
            }
            ((Starter)bot).UseArgs();
        }

        public Creature GetChild ()
        {
            var c = this.Clone() as Creature;
            c.points = 0;
            for (int i = 0; i < ((Starter)c.bot).args.Length; i++)
            {
                if (Utils.RandomFloat(0, 1) < 0.2f)
                {
                    ((Starter)c.bot).args[i].ApplyGaussian();
                }
            }
            ((Starter)c.bot).UseArgs();
            return c;
        }
    }
}
