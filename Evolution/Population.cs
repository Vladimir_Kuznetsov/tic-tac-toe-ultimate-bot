﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIGames.UltimateTicTacToe.StarterBot.Communication;

namespace AIGames.UltimateTicTacToe.StarterBot.Evolution
{
    public class Population
    {
        Creature adam;
        public List<Creature> creatures;
        public List<Creature> bestFighters;

        // Create Adam (the first bot)
        public Population (LimitedVal[] args)
        {
            creatures = new List<Creature>();
            bestFighters = new List<Creature>();
            adam = new Creature(args);
        }

        // Make a population of n random creatures
        public void InitRandom (int n)
        {
            creatures.Clear();

            // The first creature is Adam's clone
            creatures.Add(adam.Clone() as Creature);

            // Other creatures are random
            for (int i = 1; i < n; i++)
            {
                var newCreature = new Creature(Utils.ArrayDeepCopy<LimitedVal>(((Starter)adam.bot).args));
                newCreature.RandomizeGenes();
                creatures.Add(newCreature);
            }
        }

        // Make a population of n Adam children
        public void InitWithChildren (int n)
        {
            creatures.Clear();

            creatures.Add(adam.Clone() as Creature);

            for (int i = 1; i < n; i++)
                creatures.Add(adam.GetChild());
        }

        // Get next generation
        public void NextGeneration()
        {
            var oldGeneration = new List<Creature>();
            foreach (var c in creatures)
                oldGeneration.Add(c.Clone() as Creature);

            // Save old generation and sort it, the fittest is the first one
            oldGeneration.Sort((c1, c2) => (c2.points + Utils.RandomFloat(0, 0.1f)).CompareTo(c1.points + Utils.RandomFloat(0, 0.1f)));
            creatures.Clear();

            // Make the best immortal
            if (!bestFighters.Select<Creature, string>(c => c.bot.GetName()).Contains(oldGeneration[0].bot.GetName()))
                bestFighters.Add(oldGeneration[0]);

            // Save 3 best for the next generation
            for (int i = 0; i < 3; i++)
            {
                creatures.Add(oldGeneration[i].Clone() as Creature);
            }

            // Add 7 random immortal fighters
            int numOfBestFighters = Math.Min(10, bestFighters.Count);
            for (int i = 3; i < numOfBestFighters; i++)
            {
                creatures.Add(bestFighters[Utils.RandomInt(0, bestFighters.Count)].Clone() as Creature);
            }

            // Add 10 strangers
            for (int i = numOfBestFighters; i < 20; i++)
            {
                var newCreature = new Creature(Utils.ArrayDeepCopy<LimitedVal>(((Starter)adam.bot).args));
                newCreature.RandomizeGenes();
                creatures.Add(newCreature); 
            }

            // Add kids
            for (int i = 20; i < oldGeneration.Count; i++)
            {
                var child = GetRandomCreatureByFitness(oldGeneration).GetChild();
                creatures.Add(child);
            }

            // Reset points
            foreach (var c in creatures)
                c.points = 0;

        }

        // Get a random creature. Fitter ones are more probable
        private static Creature GetRandomCreatureByFitness (List<Creature> cl)
        {
            int priority = cl.Count;

            // Weight is proportional to the rating position in power of N
            // this allows the fittest creatures to have more children
            float N = 4f;
            float[] weights = cl.Select<Creature, float>(x => x.points * (float) Math.Pow(priority--, N)).ToArray<float>();
            float randomVal = Utils.RandomFloat(0, weights.Sum());
            int i = 0;
            while (randomVal > 0 && i < cl.Count)
                randomVal -= weights[i++];
            i--;
            return cl[i];
        }
    }
}
