﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using AIGames.UltimateTicTacToe.StarterBot.Communication;

namespace AIGames.UltimateTicTacToe.StarterBot
{
    static class Strategy
    {
        public static readonly float STRATEGY_VALUE = TinyBoard.GAME_OVER_VALUE / 6;
        
        public static readonly float[] defaultVals = new float[] { 0.4f * STRATEGY_VALUE, 0.3f * STRATEGY_VALUE, 0.4f * STRATEGY_VALUE,
                                                       0.3f * STRATEGY_VALUE, 1f * STRATEGY_VALUE, 0.3f * STRATEGY_VALUE,
                                                       0.4f * STRATEGY_VALUE, 0.3f * STRATEGY_VALUE, 0.4f * STRATEGY_VALUE };
                                                       
        /// <summary>Calculate strategy value of each tiny board</summary>
        public static void DoStrategy(MacroBoard macro, byte player, Settings s)
        {
            List<Line>[] lines = new List<Line>[] { new List<Line>(), new List<Line>() };

            GetRows(macro, ref lines);
            GetColumns(macro, ref lines);
            GetDiagonals(macro, ref lines);

            // Define individual strategies for each tiny board
            // Initially we don't care about any game boards
            foreach (var b in macro.boards)
                b.tinyStrategy = TinyStrategy.dontCare;
            for (int i = 0; i < 2; i++)
                foreach (var l in lines[i])
                {
                    // If a board can be used to build a line, we apply basic strategy
                    foreach (var c in l.cells)
                        if (macro.boards[c].tinyStrategy == TinyStrategy.dontCare)
                            macro.boards[c].tinyStrategy = TinyStrategy.basic;

                    if (l.lastCell >= 0 && macro.boards[l.lastCell].tinyStrategy != TinyStrategy.blockOpponent)
                        // If this is the last cell in a line for player, win this board
                        if (i + 1 == player)
                        {
                            macro.boards[l.lastCell].tinyStrategy = TinyStrategy.winBoard;
                        // If this is the last cell in a line for opponent, make it tie or win
                        }
                        else
                        {
                            macro.boards[l.lastCell].tinyStrategy = TinyStrategy.blockOpponent;
                        }
                }

            // Exponential weight curve
            for (int i = 0; i < 2; i++)
                foreach (var l in lines[i])
                    l.val = (float)Math.Pow(l.val, s.STRATEGY_LINE_POW_K.val);
            
            // Sort lines by value
            lines[0].Sort();
            lines[1].Sort();
            
            // New macroStrategy values
            float[] newVals = defaultVals.Clone() as float[];

            // Apply weights for player 1
            for (int i = 0; i < lines[0].Count; i++)
                foreach (var cell in lines[0][i].cells)
                    newVals[cell] += lines[0][i].val * s.STRATEGY_LINE_K.val;

            // Apply weights for player 2
            for (int i = 0; i < lines[1].Count; i++)
                foreach (var cell in lines[1][i].cells)
                    newVals[cell] += lines[1][i].val * s.STRATEGY_LINE_K.val;

            macro.SetMacroStrategies(newVals);
            SetTraps(macro, s);
        }

        /// <summary>Returns list of rows with scores higher then minValue for each player</summary>
        private static void GetRows(MacroBoard macro, ref List<Line>[] lines)
        {
            // Check rows
            for (int i = 0; i < 3; i++)
            {
                List<int> requiredCells1 = new List<int>();
                List<int> requiredCells2 = new List<int>();
                float valueForPlayer1 = 0, valueForPlayer2 = 0;
                for (int j = 0; j < 3; j++)
                {
                    int b = i * 3 + j;
                    switch (macro.boards[b].winner) {
                        case -1:
                            if (macro.boards[b].canWin1 == false)
                            {
                                valueForPlayer1 = 0;
                                requiredCells1.Clear();
                                j = 3;
                                break;
                            }
                            valueForPlayer1 += macro.boards[b].valueForPlayer1;
                            requiredCells1.Add(b);
                            break;
                        case 1:
                            valueForPlayer1 += macro.boards[b].valueForPlayer1;
                            break;
                        case 0:
                        case 2:
                            valueForPlayer1 = 0;
                            requiredCells1.Clear();
                            j = 3;
                            break;
                    }
                }
                for (int j = 0; j < 3; j++)
                {
                    int b = i * 3 + j;
                    switch (macro.boards[b].winner)
                    {
                        case -1:
                            if (macro.boards[b].canWin2 == false)
                            {
                                valueForPlayer2 = 0;
                                requiredCells2.Clear();
                                j = 3;
                                break;
                            }
                            valueForPlayer2 += macro.boards[b].valueForPlayer2;
                            requiredCells2.Add(b);
                            break;
                        case 2:
                            valueForPlayer2 += macro.boards[b].valueForPlayer2;
                            break;
                        case 0:
                        case 1:
                            valueForPlayer2 = 0;
                            requiredCells2.Clear();
                            j = 3;
                            break;
                    }
                }

                /*
                // Analyze available lines
                Console.WriteLine("row: " + i + ", cells: " + (i * 3)
                                                    + ", " + (i * 3 + 1)
                                                    + ", " + (i * 3 + 2)
                                                    + ", p1 val: " + valueForPlayer1
                                                    + ", p2 val: " + valueForPlayer2);
                */
                
                if (valueForPlayer1 > 0)
                    lines[0].Add(new Line(i * 3, i * 3 + 1, i * 3 + 2, valueForPlayer1, requiredCells1.Count == 1 ? requiredCells1[0] : -1));
                if (valueForPlayer2 > 0)
                    lines[1].Add(new Line(i * 3, i * 3 + 1, i * 3 + 2, valueForPlayer2, requiredCells2.Count == 1 ? requiredCells2[0] : -1));
            }
        }

        /// <summary>Returns list of columns with scores higher then minValue for each player</summary>
        private static void GetColumns(MacroBoard macro, ref List<Line>[] lines)
        {
            // Check coumns
            for (int i = 0; i < 3; i++)
            {
                List<int> requiredCells1 = new List<int>();
                List<int> requiredCells2 = new List<int>();
                float valueForPlayer1 = 0, valueForPlayer2 = 0;
                for (int j = 0; j < 3; j++)
                {
                    int b = j * 3 + i;
                    switch (macro.boards[b].winner)
                    {
                        case -1:
                            if (macro.boards[b].canWin1 == false)
                            {
                                valueForPlayer1 = 0;
                                requiredCells1.Clear();
                                j = 3;
                                break;
                            }
                            valueForPlayer1 += macro.boards[b].valueForPlayer1;
                            requiredCells1.Add(b);
                            break;
                        case 1:
                            valueForPlayer1 += macro.boards[b].valueForPlayer1;
                            break;
                        case 0:
                        case 2:
                            valueForPlayer1 = 0;
                            requiredCells1.Clear();
                            j = 3;
                            break;
                    }
                }
                for (int j = 0; j < 3; j++)
                {
                    int b = j * 3 + i;
                    switch (macro.boards[b].winner)
                    {
                        case -1:
                            if (macro.boards[b].canWin2 == false)
                            {
                                valueForPlayer2 = 0;
                                requiredCells2.Clear();
                                j = 3;
                                break;
                            }
                            valueForPlayer2 += macro.boards[b].valueForPlayer2;
                            requiredCells2.Add(b);
                            break;
                        case 2:
                            valueForPlayer2 += macro.boards[b].valueForPlayer2;
                            break;
                        case 0:
                        case 1:
                            valueForPlayer2 = 0;
                            requiredCells2.Clear();
                            j = 3;
                            break;
                    }
                }

                /*
                // Analyze available lines
                Console.WriteLine("column: " + i + ", cells: " + (i)
                                                    + ", " + (i + 3)
                                                    + ", " + (i + 6)
                                                    + ", p1 val: " + valueForPlayer1
                                                    + ", p2 val: " + valueForPlayer2);
                */

                if (valueForPlayer1 > 0)
                    lines[0].Add(new Line(i, i + 3, i + 6, valueForPlayer1, requiredCells1.Count == 1? requiredCells1[0] : -1));
                if (valueForPlayer2 > 0)
                    lines[1].Add(new Line(i, i + 3, i + 6, valueForPlayer2, requiredCells2.Count == 1? requiredCells2[0] : -1));
            }
        }

        /// <summary>Returns list of diagonals with scores higher then minValue for each player</summary>
        private static void GetDiagonals(MacroBoard macro, ref List<Line>[] lines)
        {
            // Check diagonals
            int[,] d = new int[,]
            {
                { 0, 4, 8 },
                { 2, 4, 6 }
            };

            for (int i = 0; i < 2; i++)
            {
                List<int> requiredCells1 = new List<int>();
                List<int> requiredCells2 = new List<int>();
                float valueForPlayer1 = 0, valueForPlayer2 = 0;
                for (int j = 0; j < 3; j++)
                {
                    switch (macro.boards[d[i, j]].winner)
                    {
                        case -1:
                            if (macro.boards[d[i, j]].canWin1 == false)
                            {
                                valueForPlayer1 = 0;
                                requiredCells1.Clear();
                                j = 3;
                                break;
                            }
                            valueForPlayer1 += macro.boards[d[i, j]].valueForPlayer1;
                            requiredCells1.Add(d[i, j]);
                            break;
                        case 1:
                            valueForPlayer1 += macro.boards[d[i, j]].valueForPlayer1;
                            break;
                        case 0:
                        case 2:
                            valueForPlayer1 = 0;
                            requiredCells1.Clear();
                            j = 3;
                            break;
                    }
                }
                for (int j = 0; j < 3; j++)
                {
                    switch (macro.boards[d[i, j]].winner)
                    {
                        case -1:
                            if (macro.boards[d[i, j]].canWin2 == false)
                            {
                                valueForPlayer2 = 0;
                                requiredCells2.Clear();
                                j = 3;
                                break;
                            }
                            valueForPlayer2 += macro.boards[d[i, j]].valueForPlayer2;
                            requiredCells2.Add(d[i, j]);
                            break;
                        case 2:
                            valueForPlayer2 += macro.boards[d[i, j]].valueForPlayer2;
                            break;
                        case 0:
                        case 1:
                            valueForPlayer2 = 0;
                            requiredCells2.Clear();
                            j = 3;
                            break;
                    }
                }

                /*
                // Analyze available lines
                Console.WriteLine("diagonal: " + i + ", cells: " + (d[i, 0])
                                                    + ", " + d[i, 1]
                                                    + ", " + d[i, 2]
                                                    + ", p1 val: " + valueForPlayer1
                                                    + ", p2 val: " + valueForPlayer2);
                */

                if (valueForPlayer1 > 0)
                    lines[0].Add(new Line(d[i, 0], d[i, 1], d[i, 2], valueForPlayer1, requiredCells1.Count == 1? requiredCells1[0] : -1));
                if (valueForPlayer2 > 0)
                    lines[1].Add(new Line(d[i, 0], d[i, 1], d[i, 2], valueForPlayer2, requiredCells2.Count == 1? requiredCells2[0] : -1));
            }
        }

        /// <summary>Look for possible traps and set them</summary>
        private static void SetTraps(MacroBoard macro, Settings s)
        {
            // If the game is not intense enough, don't set traps
            int intensity = 0;
            foreach (var b in macro.boards)
                if (b.winner != -1)
                    intensity++;
            if (intensity < 2)
                return;

            // If we have more than one must win board, we can use one of them as a trap
            int mustWinBoards = 0;
            foreach (var b in macro.boards)
                mustWinBoards += (b.tinyStrategy == TinyStrategy.winBoard) ? 1 : 0;

            // Check each board if it meets all of the requirements to be a trap
            for (int i = 0; i < 9; i++)
                if (macro.boards[i].winner == -1 && macro.boards[i].tinyStrategy != TinyStrategy.blockOpponent
                    && (macro.boards[i].tinyStrategy != TinyStrategy.winBoard || mustWinBoards > 1))
                {
                    TinyBoard b = macro.boards[i];
                    bool isTrap = false;
                    int numberOfDesiredCells = 0;

                    // Check every empty cell on a selected board
                    List<int> emptyCells = b.GetEmptyCells();
                    foreach (var c in emptyCells)
                    {
                        // If opponent can send me to the trap from inside of the trap, it's a bad trap
                        if (c == i)
                        {
                            isTrap = false;
                            break;
                        }

                        // If there is an escape from selected board, it's a bad trap
                        if (macro.boards[c].winner == -1 && (macro.boards[c].tinyStrategy == TinyStrategy.basic
                                        || macro.boards[c].tinyStrategy == TinyStrategy.dontCare) && macro.boards[c].board[i] != 0)
                        {
                            isTrap = false;
                            break;
                        }

                        // If there is no desired board references, we're not interested in this trap
                        if (macro.boards[c].tinyStrategy == TinyStrategy.blockOpponent ||
                            macro.boards[c].tinyStrategy == TinyStrategy.winBoard ||
                            macro.boards[c].winner != -1)
                        {
                            numberOfDesiredCells++;
                            isTrap = true;
                        }
                    }

                    // Set trap value
                    if (isTrap)
                    {
                        b.macroStrategy -= s.TRAP_K.val * TinyBoard.GAME_OVER_VALUE * numberOfDesiredCells / emptyCells.Count;
                        b.macroStrategy = Math.Max(b.macroStrategy, 0.0001f);
                        Console.Error.WriteLine("Setting trap on B" + b.boardNum + ", value: " + (2 * TinyBoard.GAME_OVER_VALUE * numberOfDesiredCells / emptyCells.Count));
                    }
                }
        }

        /// <summary>Sets default strategy for an empty field</summary>
        public static void SetDefaultStrategy(MacroBoard macro)
        {
            macro.SetMacroStrategies(defaultVals);
        }
    }
}
